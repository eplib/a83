<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83444">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Tuesday the fifth of September, 1654. Resolved by the Parliament, that no petition against any election ...</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83444 of text R212084 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.19[13]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83444</idno>
    <idno type="STC">Wing E2256C</idno>
    <idno type="STC">Thomason 669.f.19[13]</idno>
    <idno type="STC">ESTC R212084</idno>
    <idno type="EEBO-CITATION">99870735</idno>
    <idno type="PROQUEST">99870735</idno>
    <idno type="VID">163351</idno>
    <idno type="PROQUESTGOID">2240951770</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83444)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163351)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f19[13])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Tuesday the fifth of September, 1654. Resolved by the Parliament, that no petition against any election ...</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by John Field, Printer to the Parliament of England,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1655 [i.e. 1654]</date>
     </publicationStmt>
     <notesStmt>
      <note>Title from caption and first lines of text.</note>
      <note>Annotation on Thomason copy: the second '5' in imprint year crossed out and replaced with "4".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Elections -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83444</ep:tcp>
    <ep:estc> R212084</ep:estc>
    <ep:stc> (Thomason 669.f.19[13]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Tuesday the fifth of September, 1654. Resolved by the Parliament, that no petition against any election ...</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1654</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>84</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-10</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83444-e10">
  <body xml:id="A83444-e20">
   <pb facs="tcp:163351:1" rend="simple:additions" xml:id="A83444-001-a"/>
   <div type="Parliamentary_resolution" xml:id="A83444-e30">
    <head xml:id="A83444-e40">
     <figure xml:id="A83444-e50">
      <figDesc xml:id="A83444-e60">blazon or coat of arms</figDesc>
     </figure>
    </head>
    <opener xml:id="A83444-e70">
     <dateline xml:id="A83444-e80">
      <date xml:id="A83444-e90">
       <w lemma="Tuesday" pos="nn1" rend="hi" xml:id="A83444-001-a-0010">Tuesday</w>
       <w lemma="the" pos="d" xml:id="A83444-001-a-0020">the</w>
       <w lemma="five" pos="ord" xml:id="A83444-001-a-0030">Fifth</w>
       <w lemma="of" pos="acp" xml:id="A83444-001-a-0040">of</w>
       <hi xml:id="A83444-e110">
        <w lemma="September" pos="nn1" xml:id="A83444-001-a-0050">September</w>
        <pc xml:id="A83444-001-a-0060">,</pc>
       </hi>
       <w lemma="1654." pos="crd" xml:id="A83444-001-a-0070">1654.</w>
       <pc unit="sentence" xml:id="A83444-001-a-0080"/>
      </date>
     </dateline>
     <lb xml:id="A83444-e120"/>
     <hi xml:id="A83444-e130">
      <w lemma="resolve" pos="j-vn" xml:id="A83444-001-a-0090">Resolved</w>
      <w lemma="by" pos="acp" xml:id="A83444-001-a-0100">by</w>
      <w lemma="the" pos="d" xml:id="A83444-001-a-0110">the</w>
      <w lemma="parliament" pos="n1" xml:id="A83444-001-a-0120">Parliament</w>
      <pc xml:id="A83444-001-a-0130">,</pc>
     </hi>
    </opener>
    <p xml:id="A83444-e140">
     <w lemma="that" pos="cs" rend="decorinit" xml:id="A83444-001-a-0140">THat</w>
     <w lemma="no" pos="dx" xml:id="A83444-001-a-0150">no</w>
     <w lemma="petition" pos="n1" xml:id="A83444-001-a-0160">Petition</w>
     <w lemma="against" pos="acp" xml:id="A83444-001-a-0170">against</w>
     <w lemma="any" pos="d" xml:id="A83444-001-a-0180">any</w>
     <w lemma="election" pos="n1" xml:id="A83444-001-a-0190">Election</w>
     <w lemma="of" pos="acp" xml:id="A83444-001-a-0200">of</w>
     <w lemma="such" pos="d" xml:id="A83444-001-a-0210">such</w>
     <w lemma="member" pos="n2" xml:id="A83444-001-a-0220">Members</w>
     <w lemma="as" pos="acp" xml:id="A83444-001-a-0230">as</w>
     <w lemma="be" pos="vvb" xml:id="A83444-001-a-0240">are</w>
     <w lemma="already" pos="av" xml:id="A83444-001-a-0250">already</w>
     <w lemma="return" pos="vvn" xml:id="A83444-001-a-0260">returned</w>
     <w lemma="for" pos="acp" xml:id="A83444-001-a-0270">for</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A83444-001-a-0280">England</w>
     <w lemma="or" pos="cc" xml:id="A83444-001-a-0290">or</w>
     <hi xml:id="A83444-e160">
      <w lemma="Scotland" pos="nn1" xml:id="A83444-001-a-0300">Scotland</w>
      <pc xml:id="A83444-001-a-0310">,</pc>
     </hi>
     <w lemma="shall" pos="vmb" xml:id="A83444-001-a-0320">shall</w>
     <w lemma="be" pos="vvi" xml:id="A83444-001-a-0330">be</w>
     <w lemma="receive" pos="vvn" xml:id="A83444-001-a-0340">received</w>
     <w lemma="by" pos="acp" xml:id="A83444-001-a-0350">by</w>
     <w lemma="the" pos="d" xml:id="A83444-001-a-0360">the</w>
     <w lemma="committee" pos="n1" xml:id="A83444-001-a-0370">Committee</w>
     <w lemma="for" pos="acp" xml:id="A83444-001-a-0380">for</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A83444-001-a-0390">Priviledges</w>
     <w lemma="after" pos="acp" xml:id="A83444-001-a-0400">after</w>
     <w lemma="three" pos="crd" xml:id="A83444-001-a-0410">three</w>
     <w lemma="week" pos="n2" xml:id="A83444-001-a-0420">weeks</w>
     <w lemma="from" pos="acp" xml:id="A83444-001-a-0430">from</w>
     <w lemma="this" pos="d" xml:id="A83444-001-a-0440">this</w>
     <w lemma="day" pos="n1" xml:id="A83444-001-a-0450">day</w>
     <pc unit="sentence" xml:id="A83444-001-a-0460">.</pc>
    </p>
    <div type="license" xml:id="A83444-e170">
     <p xml:id="A83444-e180">
      <w lemma="order" pos="j-vn" xml:id="A83444-001-a-0470">ORdered</w>
      <w lemma="by" pos="acp" xml:id="A83444-001-a-0480">by</w>
      <w lemma="the" pos="d" xml:id="A83444-001-a-0490">the</w>
      <w lemma="parliament" pos="n1" xml:id="A83444-001-a-0500">Parliament</w>
      <pc xml:id="A83444-001-a-0510">,</pc>
      <w lemma="that" pos="cs" xml:id="A83444-001-a-0520">That</w>
      <w lemma="this" pos="d" xml:id="A83444-001-a-0530">this</w>
      <w lemma="vote" pos="n1" xml:id="A83444-001-a-0540">Vote</w>
      <w lemma="be" pos="vvi" xml:id="A83444-001-a-0550">be</w>
      <w lemma="print" pos="vvn" xml:id="A83444-001-a-0560">Printed</w>
      <w lemma="and" pos="cc" xml:id="A83444-001-a-0570">and</w>
      <w lemma="publish" pos="vvn" xml:id="A83444-001-a-0580">Published</w>
      <pc unit="sentence" xml:id="A83444-001-a-0590">.</pc>
     </p>
     <closer xml:id="A83444-e190">
      <signed xml:id="A83444-e200">
       <w lemma="hen" pos="ab" xml:id="A83444-001-a-0600">Hen:</w>
       <w lemma="Scobell" pos="nn1" xml:id="A83444-001-a-0620">Scobell</w>
       <pc xml:id="A83444-001-a-0630">,</pc>
       <w lemma="clerk" pos="n1" xml:id="A83444-001-a-0640">Clerk</w>
       <w lemma="of" pos="acp" xml:id="A83444-001-a-0650">of</w>
       <w lemma="the" pos="d" xml:id="A83444-001-a-0660">the</w>
       <w lemma="parliament" pos="n1" xml:id="A83444-001-a-0670">Parliament</w>
       <pc unit="sentence" xml:id="A83444-001-a-0680">.</pc>
      </signed>
     </closer>
    </div>
   </div>
  </body>
  <back xml:id="A83444-e210">
   <div type="colophon" xml:id="A83444-e220">
    <p xml:id="A83444-e230">
     <hi xml:id="A83444-e240">
      <w lemma="london" pos="nn1" xml:id="A83444-001-a-0690">London</w>
      <pc xml:id="A83444-001-a-0700">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A83444-001-a-0710">Printed</w>
     <w lemma="by" pos="acp" xml:id="A83444-001-a-0720">by</w>
     <hi xml:id="A83444-e250">
      <w lemma="John" pos="nn1" xml:id="A83444-001-a-0730">John</w>
      <w lemma="field" pos="nn1" xml:id="A83444-001-a-0740">Field</w>
      <pc xml:id="A83444-001-a-0750">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A83444-001-a-0760">Printer</w>
     <w lemma="to" pos="acp" xml:id="A83444-001-a-0770">to</w>
     <w lemma="the" pos="d" xml:id="A83444-001-a-0780">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83444-001-a-0790">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A83444-001-a-0800">of</w>
     <hi xml:id="A83444-e260">
      <w lemma="England" pos="nn1" xml:id="A83444-001-a-0810">England</w>
      <pc xml:id="A83444-001-a-0820">,</pc>
     </hi>
     <w lemma="1655." pos="crd" xml:id="A83444-001-a-0830">1655.</w>
     <pc unit="sentence" xml:id="A83444-001-a-0840"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
