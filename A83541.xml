<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83541">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Two ordinances of the Lords and Commons in Parliament assembled 26. July 1647.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83541 of text R210572 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.11[50]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83541</idno>
    <idno type="STC">Wing E2398</idno>
    <idno type="STC">Thomason 669.f.11[50]</idno>
    <idno type="STC">ESTC R210572</idno>
    <idno type="EEBO-CITATION">99869357</idno>
    <idno type="PROQUEST">99869357</idno>
    <idno type="VID">162699</idno>
    <idno type="PROQUESTGOID">2240948977</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83541)</note>
    <note>Transcribed from: (Early English Books Online ; image set 162699)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f11[50])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Two ordinances of the Lords and Commons in Parliament assembled 26. July 1647.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>printed for Mathew Walebanck,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1647.</date>
     </publicationStmt>
     <notesStmt>
      <note>Both signed: H. Elsynge Cler. Parl. D. Com.</note>
      <note>Ordinance of 23 July for settling of the militia revoked. The ordinance of 4 May 1647 to remain in force. Ordinance of 24 July declaring traitors those who get subscriptions to the engagement is null and void -- Cf. Steele.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
     <term>London (England) -- History -- 17th century -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83541</ep:tcp>
    <ep:estc> R210572</ep:estc>
    <ep:stc> (Thomason 669.f.11[50]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Two ordinances of the Lords and Commons in Parliament assembled 26. July 1647.</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1647</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>208</ep:wordCount>
    <ep:defectiveTokenCount>7</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>337</ep:defectRate>
    <ep:finalGrade>F</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 337 defects per 10,000 words puts this text in the F category of texts with  100 or more defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-11</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-11</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83541-e10">
  <body xml:id="A83541-e20">
   <pb facs="tcp:162699:1" rend="simple:additions" xml:id="A83541-001-a"/>
   <head xml:id="A83541-e30">
    <w lemma="two" pos="crd" xml:id="A83541-001-a-0010">TWO</w>
    <w lemma="ordinance" pos="n2" xml:id="A83541-001-a-0020">ORDINANCES</w>
    <w lemma="of" pos="acp" xml:id="A83541-001-a-0030">Of</w>
    <w lemma="the" pos="d" xml:id="A83541-001-a-0040">the</w>
    <w lemma="lord" pos="n2" xml:id="A83541-001-a-0050">Lords</w>
    <w lemma="and" pos="cc" xml:id="A83541-001-a-0060">and</w>
    <w lemma="commons" pos="n2" xml:id="A83541-001-a-0070">Commons</w>
    <w lemma="in" pos="acp" reg="in" xml:id="A83541-001-a-0080">Jn</w>
    <w lemma="parliament" pos="n1" xml:id="A83541-001-a-0090">PARLIAMENT</w>
    <w lemma="assemble" pos="vvn" xml:id="A83541-001-a-0100">Assembled</w>
    <w lemma="26" pos="crd" xml:id="A83541-001-a-0110">26</w>
    <pc xml:id="A83541-001-a-0120">▪</pc>
    <w lemma="July" pos="nn1" rend="hi" xml:id="A83541-001-a-0130">July</w>
    <w lemma="1647" pos="crd" xml:id="A83541-001-a-0140">1647</w>
    <pc xml:id="A83541-001-a-0150">▪</pc>
   </head>
   <div type="ordinance" xml:id="A83541-e50">
    <head xml:id="A83541-e60">
     <date xml:id="A83541-e70">
      <w lemma="july" pos="nn1" reg="July" rend="hi" xml:id="A83541-001-a-0160">Iuly</w>
      <w lemma="26" pos="crd" xml:id="A83541-001-a-0170">26</w>
      <w lemma="1647" pos="crd" xml:id="A83541-001-a-0180">1647</w>
      <pc xml:id="A83541-001-a-0190">▪</pc>
     </date>
    </head>
    <p xml:id="A83541-e90">
     <w lemma="be" pos="vvb" reg="Be" xml:id="A83541-001-a-0200">BEE</w>
     <w lemma="it" pos="pn" xml:id="A83541-001-a-0210">it</w>
     <w lemma="order" pos="vvd" xml:id="A83541-001-a-0220">Ordered</w>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-0230">and</w>
     <w lemma="ordain" pos="vvn" xml:id="A83541-001-a-0240">Ordained</w>
     <w lemma="by" pos="acp" xml:id="A83541-001-a-0250">by</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0260">the</w>
     <w lemma="lord" pos="n2" xml:id="A83541-001-a-0270">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-0280">and</w>
     <w lemma="commons" pos="n2" xml:id="A83541-001-a-0290">Commons</w>
     <w lemma="in" pos="acp" xml:id="A83541-001-a-0300">in</w>
     <w lemma="parliament" pos="n1" xml:id="A83541-001-a-0310">Parliament</w>
     <w lemma="assemble" pos="vvn" xml:id="A83541-001-a-0320">assembled</w>
     <pc xml:id="A83541-001-a-0330">,</pc>
     <w lemma="that" pos="cs" xml:id="A83541-001-a-0340">That</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0350">the</w>
     <w lemma="ordinance" pos="n1" xml:id="A83541-001-a-0360">Ordinance</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0370">of</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0380">the</w>
     <w lemma="23." pos="crd" xml:id="A83541-001-a-0390">23.</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0400">of</w>
     <w lemma="this" pos="d" xml:id="A83541-001-a-0410">this</w>
     <w lemma="instant" pos="j" xml:id="A83541-001-a-0420">instant</w>
     <w lemma="July" pos="nn1" rend="hi" xml:id="A83541-001-a-0430">July</w>
     <w lemma="for" pos="acp" xml:id="A83541-001-a-0440">for</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0450">the</w>
     <w lemma="settle" pos="n1-vg" reg="settling" xml:id="A83541-001-a-0460">setling</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0470">of</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0480">the</w>
     <w lemma="militia" pos="n1" xml:id="A83541-001-a-0490">Militia</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0500">of</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0510">the</w>
     <w lemma="city" pos="n1" xml:id="A83541-001-a-0520">City</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0530">of</w>
     <hi xml:id="A83541-e110">
      <w lemma="London" pos="nn1" xml:id="A83541-001-a-0540">London</w>
      <pc xml:id="A83541-001-a-0550">,</pc>
     </hi>
     <w lemma="be" pos="vvb" reg="be" xml:id="A83541-001-a-0560">bee</w>
     <pc xml:id="A83541-001-a-0570">,</pc>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-0580">and</w>
     <w lemma="shall" pos="vmb" xml:id="A83541-001-a-0590">shall</w>
     <w lemma="be" pos="vvi" xml:id="A83541-001-a-0600">be</w>
     <w lemma="hereby" pos="av" xml:id="A83541-001-a-0610">hereby</w>
     <w lemma="revoke" pos="vvn" xml:id="A83541-001-a-0620">revoked</w>
     <pc xml:id="A83541-001-a-0630">,</pc>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-0640">and</w>
     <w lemma="make" pos="vvd" xml:id="A83541-001-a-0650">made</w>
     <w lemma="void" pos="j" reg="void" xml:id="A83541-001-a-0660">voyd</w>
     <w lemma="to" pos="acp" xml:id="A83541-001-a-0670">to</w>
     <w lemma="all" pos="d" xml:id="A83541-001-a-0680">all</w>
     <w lemma="intent" pos="n2" xml:id="A83541-001-a-0690">intents</w>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-0700">and</w>
     <w lemma="purpose" pos="n2" xml:id="A83541-001-a-0710">purposes</w>
     <pc xml:id="A83541-001-a-0720">,</pc>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-0730">and</w>
     <w lemma="that" pos="cs" xml:id="A83541-001-a-0740">that</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0750">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83541-001-a-0760">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A83541-001-a-0770">Ordinance</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0780">of</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0790">the</w>
     <w lemma="four" pos="ord" xml:id="A83541-001-a-0800">4th</w>
     <pc xml:id="A83541-001-a-0810">,</pc>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0820">of</w>
     <w lemma="May" pos="nn1" rend="hi" xml:id="A83541-001-a-0830">May</w>
     <w lemma="1647" pos="crd" xml:id="A83541-001-a-0840">1647</w>
     <w lemma="for" pos="acp" xml:id="A83541-001-a-0850">for</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-0860">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83541-001-a-0870">said</w>
     <w lemma="militia" pos="n1" xml:id="A83541-001-a-0880">Militia</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-0890">of</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A83541-001-a-0900">London</w>
     <w lemma="be" pos="vvi" xml:id="A83541-001-a-0910">be</w>
     <w lemma="in" pos="acp" xml:id="A83541-001-a-0920">in</w>
     <w lemma="full" pos="j" xml:id="A83541-001-a-0930">full</w>
     <w lemma="force" pos="n1" xml:id="A83541-001-a-0940">force</w>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-0950">and</w>
     <w lemma="virtue" pos="n1" reg="virtue" xml:id="A83541-001-a-0960">vertue</w>
     <pc xml:id="A83541-001-a-0970">,</pc>
     <w lemma="any" pos="d" xml:id="A83541-001-a-0980">any</w>
     <w lemma="thing" pos="n1" xml:id="A83541-001-a-0990">thing</w>
     <w lemma="in" pos="acp" xml:id="A83541-001-a-1000">in</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1010">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83541-001-a-1020">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A83541-001-a-1030">Ordinance</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-1040">of</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1050">the</w>
     <w lemma="23th" pos="ord" xml:id="A83541-001-a-1060">23th</w>
     <pc xml:id="A83541-001-a-1070">.</pc>
     <w lemma="instant" pos="n1-j" xml:id="A83541-001-a-1080">instant</w>
     <pc xml:id="A83541-001-a-1090">,</pc>
     <w lemma="to" pos="acp" xml:id="A83541-001-a-1100">to</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1110">the</w>
     <w lemma="contrary" pos="j" xml:id="A83541-001-a-1120">contrary</w>
     <w lemma="notwithstanding" pos="acp" xml:id="A83541-001-a-1130">notwithstanding</w>
     <pc unit="sentence" xml:id="A83541-001-a-1140">.</pc>
    </p>
    <closer xml:id="A83541-e140">
     <signed xml:id="A83541-e150">
      <w lemma="h." pos="ab" xml:id="A83541-001-a-1150">H.</w>
      <w lemma="Elsing" pos="nn1" reg="Elsing" xml:id="A83541-001-a-1160">Elsynge</w>
      <pc xml:id="A83541-001-a-1170">:</pc>
      <w lemma="cler." pos="ab" xml:id="A83541-001-a-1180">Cler.</w>
      <w lemma="d" pos="sy" xml:id="A83541-001-a-1200">D</w>
      <pc xml:id="A83541-001-a-1210">▪</pc>
      <w lemma="Com:" pos="ab" xml:id="A83541-001-a-1220">Com:</w>
     </signed>
    </closer>
   </div>
   <div type="ordinance" xml:id="A83541-e160">
    <head xml:id="A83541-e170">
     <date xml:id="A83541-e180">
      <w lemma="n/a" pos="fla" xml:id="A83541-001-a-1240">Die</w>
      <w lemma="n/a" pos="ffr" xml:id="A83541-001-a-1250">Lune</w>
      <w lemma="26" pos="crd" rend="hi" xml:id="A83541-001-a-1260">26</w>
      <w lemma="n/a" pos="fla" xml:id="A83541-001-a-1270">Iulii</w>
      <w lemma="1647" pos="crd" rend="hi" xml:id="A83541-001-a-1280">1647</w>
     </date>
    </head>
    <p xml:id="A83541-e210">
     <w lemma="be" pos="vvb" xml:id="A83541-001-a-1290">BE</w>
     <w lemma="it" pos="pn" xml:id="A83541-001-a-1300">it</w>
     <w lemma="ordain" pos="vvn" reg="ordained" xml:id="A83541-001-a-1310">Ordayned</w>
     <w lemma="by" pos="acp" xml:id="A83541-001-a-1320">by</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1330">the</w>
     <w lemma="lord" pos="n2" xml:id="A83541-001-a-1340">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-1350">and</w>
     <w lemma="commons" pos="n2" xml:id="A83541-001-a-1360">Commons</w>
     <w lemma="in" pos="acp" xml:id="A83541-001-a-1370">in</w>
     <w lemma="parliament" pos="n1" xml:id="A83541-001-a-1380">Parliament</w>
     <w lemma="assemble" pos="vvn" xml:id="A83541-001-a-1390">assembled</w>
     <pc xml:id="A83541-001-a-1400">,</pc>
     <w lemma="that" pos="cs" xml:id="A83541-001-a-1410">that</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1420">the</w>
     <w lemma="declaration" pos="n1" xml:id="A83541-001-a-1430">Declaration</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-1440">of</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1450">the</w>
     <w lemma="twenty" pos="crd" reg="twenty" xml:id="A83541-001-a-1460">twentie</w>
     <w lemma="four" pos="ord" xml:id="A83541-001-a-1470">fourth</w>
     <w lemma="of" pos="acp" xml:id="A83541-001-a-1480">of</w>
     <w lemma="this" pos="d" xml:id="A83541-001-a-1490">this</w>
     <w lemma="instant" pos="j" xml:id="A83541-001-a-1500">instant</w>
     <hi xml:id="A83541-e220">
      <w lemma="July" pos="nn1" xml:id="A83541-001-a-1510">July</w>
      <pc xml:id="A83541-001-a-1520">,</pc>
     </hi>
     <w lemma="which" pos="crq" xml:id="A83541-001-a-1530">which</w>
     <w lemma="declare" pos="vvb" xml:id="A83541-001-a-1540">declare</w>
     <w lemma="all" pos="d" xml:id="A83541-001-a-1550">all</w>
     <w lemma="those" pos="d" xml:id="A83541-001-a-1560">those</w>
     <w lemma="traitor" pos="n2" reg="traitors" xml:id="A83541-001-a-1570">Traytors</w>
     <pc xml:id="A83541-001-a-1580">,</pc>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-1590">and</w>
     <w lemma="so" pos="av" xml:id="A83541-001-a-1600">so</w>
     <w lemma="to" pos="prt" xml:id="A83541-001-a-1610">to</w>
     <w lemma="forfeit" pos="vvi" xml:id="A83541-001-a-1620">forfeit</w>
     <w lemma="life" pos="n1" xml:id="A83541-001-a-1630">Life</w>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-1640">and</w>
     <w lemma="estate" pos="n1" xml:id="A83541-001-a-1650">Estate</w>
     <pc xml:id="A83541-001-a-1660">,</pc>
     <w lemma="who" pos="crq" xml:id="A83541-001-a-1670">who</w>
     <w lemma="shall" pos="vmb" xml:id="A83541-001-a-1680">shall</w>
     <w lemma="after" pos="acp" xml:id="A83541-001-a-1690">after</w>
     <w lemma="publication" pos="n1" xml:id="A83541-001-a-1700">publication</w>
     <w lemma="thereof" pos="av" xml:id="A83541-001-a-1710">thereof</w>
     <w lemma="act" pos="vvi" xml:id="A83541-001-a-1720">act</w>
     <w lemma="thereupon" pos="av" xml:id="A83541-001-a-1730">thereupon</w>
     <w lemma="to" pos="prt" xml:id="A83541-001-a-1740">to</w>
     <w lemma="get" pos="vvi" xml:id="A83541-001-a-1750">get</w>
     <w lemma="subscription" pos="n2" xml:id="A83541-001-a-1760">subscriptions</w>
     <pc xml:id="A83541-001-a-1770">,</pc>
     <w lemma="be" pos="vvb" xml:id="A83541-001-a-1780">be</w>
     <w lemma="null" pos="j" xml:id="A83541-001-a-1790">null</w>
     <w lemma="and" pos="cc" xml:id="A83541-001-a-1800">and</w>
     <w lemma="void" pos="j" xml:id="A83541-001-a-1810">void</w>
     <w lemma="any" pos="d" xml:id="A83541-001-a-1820">any</w>
     <w lemma="thing" pos="n1" xml:id="A83541-001-a-1830">thing</w>
     <w lemma="in" pos="acp" xml:id="A83541-001-a-1840">in</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1850">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83541-001-a-1860">said</w>
     <w lemma="declaration" pos="n1" xml:id="A83541-001-a-1870">Declaration</w>
     <w lemma="to" pos="acp" xml:id="A83541-001-a-1880">to</w>
     <w lemma="the" pos="d" xml:id="A83541-001-a-1890">the</w>
     <w lemma="contrary" pos="j" xml:id="A83541-001-a-1900">contrary</w>
     <w lemma="notwithstanding" pos="acp" reg="notwithstanding" xml:id="A83541-001-a-1910">notwitstanding</w>
    </p>
    <closer xml:id="A83541-e230">
     <signed xml:id="A83541-e240">
      <w lemma="H" pos="sy" xml:id="A83541-001-a-1920">H</w>
      <pc xml:id="A83541-001-a-1930">▪</pc>
      <w lemma="elsing" pos="nn1" reg="Elsing" xml:id="A83541-001-a-1940">Elsyng</w>
      <w lemma="cler." pos="ab" xml:id="A83541-001-a-1950">Cler.</w>
      <w lemma="parl" pos="ab" xml:id="A83541-001-a-1970">Parl</w>
      <pc xml:id="A83541-001-a-1980">▪</pc>
      <w lemma="d." pos="ab" xml:id="A83541-001-a-1990">D.</w>
      <w lemma="com." pos="ab" xml:id="A83541-001-a-2000">Com.</w>
      <pc unit="sentence" xml:id="A83541-001-a-2010"/>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A83541-e250">
   <div type="colophon" xml:id="A83541-e260">
    <p xml:id="A83541-e270">
     <w lemma="London" pos="nn1" xml:id="A83541-001-a-2020">London</w>
     <w lemma="print" pos="vvn" xml:id="A83541-001-a-2030">printed</w>
     <w lemma="for" pos="acp" xml:id="A83541-001-a-2040">for</w>
     <hi xml:id="A83541-e280">
      <w lemma="Matthew" pos="nn1" reg="Matthew" xml:id="A83541-001-a-2050">Mathew</w>
      <w lemma="Walebanck" pos="nn1" xml:id="A83541-001-a-2060">Walebanck</w>
     </hi>
     <w lemma="1647." pos="crd" xml:id="A83541-001-a-2070">1647.</w>
     <pc unit="sentence" xml:id="A83541-001-a-2080"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
