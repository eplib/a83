<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83029">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Die Mercurii, 22 Sept. 1647. An ordinance of the Lords and Commons assembled in Parliament, concerning sequestred books, evidences, records and writings.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83029 of text R205431 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.11[89]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83029</idno>
    <idno type="STC">Wing E1814</idno>
    <idno type="STC">Thomason 669.f.11[89]</idno>
    <idno type="STC">ESTC R205431</idno>
    <idno type="EEBO-CITATION">99864818</idno>
    <idno type="PROQUEST">99864818</idno>
    <idno type="VID">162741</idno>
    <idno type="PROQUESTGOID">2248538751</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83029)</note>
    <note>Transcribed from: (Early English Books Online ; image set 162741)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f11[89])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Die Mercurii, 22 Sept. 1647. An ordinance of the Lords and Commons assembled in Parliament, concerning sequestred books, evidences, records and writings.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet [(1) p.]</extent>
     <publicationStmt>
      <publisher>Printed for Edward Husband, Printer to the Honorable House of Commons,</publisher>
      <pubPlace>London :</pubPlace>
      <date>Sept. 28. 1647.</date>
     </publicationStmt>
     <notesStmt>
      <note>With decorative border.</note>
      <note>All books, writings, etc. sequestered in London and Westminster by ordinance of 16 November 1643 are to be placed in the custody of Henry Elsynge, who shall deliver them out on order from either House, etc. -- Cf. Steele.</note>
      <note>Order to print signed: H: Elsynge, Cler. Parl. D. Com.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Attachment and garnishment -- England -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1642-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83029</ep:tcp>
    <ep:estc> R205431</ep:estc>
    <ep:stc> (Thomason 669.f.11[89]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Die Mercurii, 22 Sept. 1647. An ordinance of the Lords and Commons assembled in Parliament, concerning sequestred books, evidences, records</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1647</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>294</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-10</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83029-e10">
  <body xml:id="A83029-e20">
   <pb facs="tcp:162741:1" rend="simple:additions" xml:id="A83029-001-a"/>
   <div type="Parliamentary_ordinance" xml:id="A83029-e30">
    <opener xml:id="A83029-e40">
     <dateline xml:id="A83029-e50">
      <date xml:id="A83029-e60">
       <foreign xml:id="A83029-e70" xml:lang="lat">
        <w lemma="n/a" pos="fla" xml:id="A83029-001-a-0010">Die</w>
        <w lemma="n/a" pos="fla" xml:id="A83029-001-a-0020">Mercurii</w>
        <pc xml:id="A83029-001-a-0030">,</pc>
       </foreign>
       <w lemma="22" pos="crd" xml:id="A83029-001-a-0040">22</w>
       <w lemma="sept." pos="ab" rend="hi" xml:id="A83029-001-a-0050">Sept.</w>
       <w lemma="1647." pos="crd" xml:id="A83029-001-a-0070">1647.</w>
       <pc unit="sentence" xml:id="A83029-001-a-0080"/>
      </date>
     </dateline>
    </opener>
    <head xml:id="A83029-e90">
     <w lemma="a" pos="d" xml:id="A83029-001-a-0090">AN</w>
     <w lemma="ordinance" pos="n1" xml:id="A83029-001-a-0100">ORDINANCE</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0110">OF</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-0120">The</w>
     <w lemma="lord" pos="n2" xml:id="A83029-001-a-0130">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-0140">and</w>
     <w lemma="commons" pos="n2" xml:id="A83029-001-a-0150">Commons</w>
     <w lemma="assemble" pos="vvn" xml:id="A83029-001-a-0160">assembled</w>
     <w lemma="in" pos="acp" xml:id="A83029-001-a-0170">in</w>
     <w lemma="parliament" pos="n1" xml:id="A83029-001-a-0180">Parliament</w>
     <pc xml:id="A83029-001-a-0190">,</pc>
     <w lemma="concerning" pos="acp" xml:id="A83029-001-a-0200">Concerning</w>
     <hi xml:id="A83029-e100">
      <w lemma="sequester" pos="j-vn" reg="sequestered" xml:id="A83029-001-a-0210">Sequestred</w>
      <w lemma="book" pos="n2" xml:id="A83029-001-a-0220">Books</w>
      <pc xml:id="A83029-001-a-0230">,</pc>
      <w lemma="evidence" pos="n2" xml:id="A83029-001-a-0240">Evidences</w>
      <pc xml:id="A83029-001-a-0250">,</pc>
      <w lemma="record" pos="n2" xml:id="A83029-001-a-0260">Records</w>
      <w lemma="and" pos="cc" xml:id="A83029-001-a-0270">and</w>
      <w lemma="writing" pos="n2" xml:id="A83029-001-a-0280">Writings</w>
      <pc unit="sentence" xml:id="A83029-001-a-0290">.</pc>
     </hi>
    </head>
    <p xml:id="A83029-e110">
     <w lemma="be" pos="vvb" xml:id="A83029-001-a-0300">BE</w>
     <w lemma="it" pos="pn" xml:id="A83029-001-a-0310">it</w>
     <w lemma="ordain" pos="vvn" xml:id="A83029-001-a-0320">Ordained</w>
     <w lemma="by" pos="acp" xml:id="A83029-001-a-0330">by</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-0340">the</w>
     <w lemma="lord" pos="n2" xml:id="A83029-001-a-0350">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-0360">and</w>
     <w lemma="commons" pos="n2" xml:id="A83029-001-a-0370">Commons</w>
     <w lemma="assemble" pos="vvn" xml:id="A83029-001-a-0380">assembled</w>
     <w lemma="in" pos="acp" xml:id="A83029-001-a-0390">in</w>
     <w lemma="parliament" pos="n1" xml:id="A83029-001-a-0400">Parliament</w>
     <pc xml:id="A83029-001-a-0410">,</pc>
     <w lemma="that" pos="cs" xml:id="A83029-001-a-0420">That</w>
     <w lemma="all" pos="d" xml:id="A83029-001-a-0430">all</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-0440">the</w>
     <w lemma="book" pos="n2" xml:id="A83029-001-a-0450">Books</w>
     <pc xml:id="A83029-001-a-0460">,</pc>
     <w lemma="evidence" pos="n2" xml:id="A83029-001-a-0470">Evidences</w>
     <pc xml:id="A83029-001-a-0480">,</pc>
     <w lemma="record" pos="n2" xml:id="A83029-001-a-0490">Records</w>
     <pc xml:id="A83029-001-a-0500">,</pc>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-0510">and</w>
     <w lemma="writing" orig="VVritings" pos="n2" xml:id="A83029-001-a-0520">Writings</w>
     <w lemma="sequester" pos="vvn" reg="sequestered" xml:id="A83029-001-a-0530">Sequestred</w>
     <w lemma="within" pos="acp" xml:id="A83029-001-a-0540">within</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-0550">the</w>
     <w lemma="city" pos="n2" xml:id="A83029-001-a-0560">Cities</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0570">of</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A83029-001-a-0580">London</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-0590">or</w>
     <hi xml:id="A83029-e130">
      <w lemma="Westminster" pos="nn1" xml:id="A83029-001-a-0600">Westminster</w>
      <pc xml:id="A83029-001-a-0610">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A83029-001-a-0620">that</w>
     <w lemma="be" pos="vvb" xml:id="A83029-001-a-0630">are</w>
     <w lemma="come" pos="vvn" xml:id="A83029-001-a-0640">come</w>
     <w lemma="to" pos="acp" xml:id="A83029-001-a-0650">to</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-0660">the</w>
     <w lemma="hand" pos="n2" xml:id="A83029-001-a-0670">hands</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0680">of</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-0690">the</w>
     <w lemma="committee" pos="n1" xml:id="A83029-001-a-0700">Committee</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0710">of</w>
     <w lemma="lord" pos="n2" xml:id="A83029-001-a-0720">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-0730">and</w>
     <w lemma="commons" pos="n2" xml:id="A83029-001-a-0740">Commons</w>
     <pc xml:id="A83029-001-a-0750">,</pc>
     <w lemma="appoint" pos="vvn" xml:id="A83029-001-a-0760">appointed</w>
     <w lemma="by" pos="acp" xml:id="A83029-001-a-0770">by</w>
     <w lemma="ordinance" pos="n1" xml:id="A83029-001-a-0780">Ordinance</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0790">of</w>
     <w lemma="parliament" pos="n1" xml:id="A83029-001-a-0800">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0810">of</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-0820">the</w>
     <w lemma="18." pos="crd" xml:id="A83029-001-a-0830">18.</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0840">of</w>
     <w lemma="Novemb." pos="ab" xml:id="A83029-001-a-0850">Novemb.</w>
     <w lemma="1643." pos="crd" xml:id="A83029-001-a-0870">1643.</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-0880">or</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0890">of</w>
     <w lemma="any" pos="d" xml:id="A83029-001-a-0900">any</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0910">of</w>
     <w lemma="they" pos="pno" xml:id="A83029-001-a-0920">them</w>
     <pc xml:id="A83029-001-a-0930">,</pc>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-0940">or</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-0950">of</w>
     <w lemma="any" pos="d" xml:id="A83029-001-a-0960">any</w>
     <w lemma="person" pos="n1" xml:id="A83029-001-a-0970">person</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-0980">or</w>
     <w lemma="person" pos="n2" xml:id="A83029-001-a-0990">persons</w>
     <w lemma="by" pos="acp" xml:id="A83029-001-a-1000">by</w>
     <w lemma="their" pos="po" xml:id="A83029-001-a-1010">their</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-1020">or</w>
     <w lemma="any" pos="d" xml:id="A83029-001-a-1030">any</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-1040">of</w>
     <w lemma="their" pos="po" xml:id="A83029-001-a-1050">their</w>
     <w lemma="direction" pos="n1" xml:id="A83029-001-a-1060">direction</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-1070">or</w>
     <w lemma="appointment" pos="n1" xml:id="A83029-001-a-1080">appointment</w>
     <pc xml:id="A83029-001-a-1090">;</pc>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-1100">or</w>
     <w lemma="which" pos="crq" xml:id="A83029-001-a-1110">which</w>
     <w lemma="shall" pos="vmd" xml:id="A83029-001-a-1120">should</w>
     <w lemma="by" pos="acp" xml:id="A83029-001-a-1130">by</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1140">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83029-001-a-1150">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A83029-001-a-1160">Ordinance</w>
     <w lemma="be" pos="vvi" xml:id="A83029-001-a-1170">be</w>
     <w lemma="deliver" pos="vvn" xml:id="A83029-001-a-1180">delivered</w>
     <w lemma="to" pos="acp" xml:id="A83029-001-a-1190">to</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1200">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83029-001-a-1210">said</w>
     <w lemma="committee" pos="n1" xml:id="A83029-001-a-1220">Committee</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-1230">or</w>
     <w lemma="any" pos="d" xml:id="A83029-001-a-1240">any</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-1250">of</w>
     <w lemma="they" pos="pno" xml:id="A83029-001-a-1260">them</w>
     <pc xml:id="A83029-001-a-1270">,</pc>
     <w lemma="to" pos="prt" xml:id="A83029-001-a-1280">to</w>
     <w lemma="be" pos="vvi" xml:id="A83029-001-a-1290">be</w>
     <w lemma="by" pos="acp" xml:id="A83029-001-a-1300">by</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1310">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83029-001-a-1320">said</w>
     <w lemma="committee" pos="n1" xml:id="A83029-001-a-1330">Committee</w>
     <w lemma="preserve" pos="vvn" xml:id="A83029-001-a-1340">preserved</w>
     <w lemma="for" pos="acp" xml:id="A83029-001-a-1350">for</w>
     <w lemma="such" pos="d" xml:id="A83029-001-a-1360">such</w>
     <w lemma="use" pos="n2" xml:id="A83029-001-a-1370">uses</w>
     <w lemma="as" pos="acp" xml:id="A83029-001-a-1380">as</w>
     <w lemma="shall" pos="vmd" xml:id="A83029-001-a-1390">should</w>
     <w lemma="be" pos="vvi" xml:id="A83029-001-a-1400">be</w>
     <w lemma="appoint" pos="vvn" xml:id="A83029-001-a-1410">appointed</w>
     <w lemma="by" pos="acp" xml:id="A83029-001-a-1420">by</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1430">the</w>
     <w lemma="house" pos="n2" xml:id="A83029-001-a-1440">Houses</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-1450">of</w>
     <w lemma="parliament" pos="n1" xml:id="A83029-001-a-1460">Parliament</w>
     <pc xml:id="A83029-001-a-1470">,</pc>
     <w lemma="be" pos="vvb" xml:id="A83029-001-a-1480">Be</w>
     <w lemma="forthwith" pos="av" xml:id="A83029-001-a-1490">forthwith</w>
     <w lemma="deliver" pos="vvn" xml:id="A83029-001-a-1500">delivered</w>
     <w lemma="unto" pos="acp" xml:id="A83029-001-a-1510">unto</w>
     <pc xml:id="A83029-001-a-1520">,</pc>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-1530">and</w>
     <w lemma="place" pos="vvn" xml:id="A83029-001-a-1540">placed</w>
     <w lemma="in" pos="acp" xml:id="A83029-001-a-1550">in</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1560">the</w>
     <w lemma="custody" pos="n1" xml:id="A83029-001-a-1570">custody</w>
     <pc xml:id="A83029-001-a-1580">,</pc>
     <w lemma="care" pos="n1" xml:id="A83029-001-a-1590">care</w>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-1600">and</w>
     <w lemma="charge" pos="n1" xml:id="A83029-001-a-1610">charge</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-1620">of</w>
     <hi xml:id="A83029-e140">
      <w lemma="Henry" pos="nn1" xml:id="A83029-001-a-1630">Henry</w>
      <w lemma="Elsing" pos="nn1" reg="Elsing" xml:id="A83029-001-a-1640">Elsynge</w>
     </hi>
     <w lemma="esquire" pos="n1" xml:id="A83029-001-a-1650">Esquire</w>
     <pc xml:id="A83029-001-a-1660">,</pc>
     <w lemma="register" pos="n1" xml:id="A83029-001-a-1670">Register</w>
     <pc xml:id="A83029-001-a-1680">,</pc>
     <w lemma="appoint" pos="vvn" xml:id="A83029-001-a-1690">appointed</w>
     <w lemma="for" pos="acp" xml:id="A83029-001-a-1700">for</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1710">the</w>
     <w lemma="sale" pos="n1" xml:id="A83029-001-a-1720">Sale</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-1730">of</w>
     <w lemma="bishop" pos="n2" xml:id="A83029-001-a-1740">Bishops</w>
     <w lemma="land" pos="n2" xml:id="A83029-001-a-1750">Lands</w>
     <pc xml:id="A83029-001-a-1760">;</pc>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-1770">And</w>
     <w lemma="that" pos="cs" xml:id="A83029-001-a-1780">that</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1790">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83029-001-a-1800">said</w>
     <w lemma="register" pos="n1" xml:id="A83029-001-a-1810">Register</w>
     <w lemma="be" pos="vvi" xml:id="A83029-001-a-1820">be</w>
     <w lemma="hereby" pos="av" xml:id="A83029-001-a-1830">hereby</w>
     <w lemma="authorize" pos="vvn" xml:id="A83029-001-a-1840">Authorized</w>
     <w lemma="by" pos="acp" xml:id="A83029-001-a-1850">by</w>
     <w lemma="himself" pos="pr" xml:id="A83029-001-a-1860">himself</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-1870">or</w>
     <w lemma="his" pos="po" xml:id="A83029-001-a-1880">his</w>
     <w lemma="deputy" pos="n2" xml:id="A83029-001-a-1890">deputies</w>
     <w lemma="to" pos="prt" xml:id="A83029-001-a-1900">to</w>
     <w lemma="deliver" pos="vvi" xml:id="A83029-001-a-1910">deliver</w>
     <w lemma="out" pos="av" xml:id="A83029-001-a-1920">out</w>
     <w lemma="any" pos="d" xml:id="A83029-001-a-1930">any</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-1940">of</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-1950">the</w>
     <w lemma="say" pos="j-vn" xml:id="A83029-001-a-1960">said</w>
     <w lemma="book" pos="n2" xml:id="A83029-001-a-1970">Books</w>
     <pc xml:id="A83029-001-a-1980">,</pc>
     <w lemma="evidence" pos="n2" xml:id="A83029-001-a-1990">Evidences</w>
     <pc xml:id="A83029-001-a-2000">,</pc>
     <w lemma="record" pos="n2" xml:id="A83029-001-a-2010">Records</w>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-2020">and</w>
     <w lemma="writing" orig="VVritings" pos="n2" xml:id="A83029-001-a-2030">Writings</w>
     <pc xml:id="A83029-001-a-2040">,</pc>
     <w lemma="according" pos="j" xml:id="A83029-001-a-2050">according</w>
     <w lemma="as" pos="acp" xml:id="A83029-001-a-2060">as</w>
     <w lemma="from" pos="acp" xml:id="A83029-001-a-2070">from</w>
     <w lemma="time" pos="n1" xml:id="A83029-001-a-2080">time</w>
     <w lemma="to" pos="acp" xml:id="A83029-001-a-2090">to</w>
     <w lemma="time" pos="n1" xml:id="A83029-001-a-2100">time</w>
     <w lemma="he" pos="pns" xml:id="A83029-001-a-2110">he</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-2120">or</w>
     <w lemma="they" pos="pns" xml:id="A83029-001-a-2130">they</w>
     <w lemma="shall" pos="vmb" xml:id="A83029-001-a-2140">shall</w>
     <w lemma="receive" pos="vvi" xml:id="A83029-001-a-2150">receive</w>
     <w lemma="order" pos="n1" xml:id="A83029-001-a-2160">Order</w>
     <w lemma="from" pos="acp" xml:id="A83029-001-a-2170">from</w>
     <w lemma="both" pos="d" xml:id="A83029-001-a-2180">both</w>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-2190">or</w>
     <w lemma="either" pos="av-d" xml:id="A83029-001-a-2200">either</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-2210">of</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-2220">the</w>
     <w lemma="house" pos="n2" xml:id="A83029-001-a-2230">Houses</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-2240">of</w>
     <w lemma="parliament" pos="n1" xml:id="A83029-001-a-2250">Parliament</w>
     <pc xml:id="A83029-001-a-2260">,</pc>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-2270">or</w>
     <w lemma="from" pos="acp" xml:id="A83029-001-a-2280">from</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-2290">the</w>
     <w lemma="committee" pos="n1" xml:id="A83029-001-a-2300">Committee</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-2310">of</w>
     <w lemma="lord" pos="n2" xml:id="A83029-001-a-2320">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-2330">and</w>
     <w lemma="commons" pos="n2" xml:id="A83029-001-a-2340">Commons</w>
     <w lemma="for" pos="acp" xml:id="A83029-001-a-2350">for</w>
     <w lemma="sequestration" pos="n2" xml:id="A83029-001-a-2360">Sequestrations</w>
     <pc xml:id="A83029-001-a-2370">,</pc>
     <w lemma="or" pos="cc" xml:id="A83029-001-a-2380">or</w>
     <w lemma="from" pos="acp" xml:id="A83029-001-a-2390">from</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-2400">the</w>
     <w lemma="commissioner" pos="n2" xml:id="A83029-001-a-2410">Commissioners</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-2420">of</w>
     <w lemma="lord" pos="n2" xml:id="A83029-001-a-2430">Lords</w>
     <w lemma="and" pos="cc" xml:id="A83029-001-a-2440">&amp;</w>
     <w lemma="commons" pos="n2" xml:id="A83029-001-a-2450">Commons</w>
     <w lemma="sit" pos="vvg" xml:id="A83029-001-a-2460">sitting</w>
     <w lemma="at" pos="acp" xml:id="A83029-001-a-2470">at</w>
     <w lemma="goldsmith" pos="ng1" reg="goldsmith's" rend="hi" xml:id="A83029-001-a-2480">Goldsmiths</w>
     <w lemma="hall" pos="n1" xml:id="A83029-001-a-2490">hall</w>
     <w lemma="respective" pos="av-j" xml:id="A83029-001-a-2500">respectively</w>
     <pc unit="sentence" xml:id="A83029-001-a-2510">.</pc>
    </p>
    <div type="license" xml:id="A83029-e160">
     <p xml:id="A83029-e170">
      <w lemma="order" pos="j-vn" xml:id="A83029-001-a-2520">Ordered</w>
      <w lemma="by" pos="acp" xml:id="A83029-001-a-2530">by</w>
      <w lemma="the" pos="d" xml:id="A83029-001-a-2540">the</w>
      <w lemma="commons" pos="n2" xml:id="A83029-001-a-2550">Commons</w>
      <w lemma="assemble" pos="vvn" xml:id="A83029-001-a-2560">assembled</w>
      <w lemma="in" pos="acp" xml:id="A83029-001-a-2570">in</w>
      <w lemma="parliament" pos="n1" xml:id="A83029-001-a-2580">Parliament</w>
      <pc xml:id="A83029-001-a-2590">,</pc>
      <w lemma="that" pos="cs" xml:id="A83029-001-a-2600">That</w>
      <w lemma="this" pos="d" xml:id="A83029-001-a-2610">this</w>
      <w lemma="ordinance" pos="n1" xml:id="A83029-001-a-2620">Ordinance</w>
      <w lemma="be" pos="vvb" xml:id="A83029-001-a-2630">be</w>
      <w lemma="forthwith" pos="av" xml:id="A83029-001-a-2640">forthwith</w>
      <w lemma="print" pos="vvn" xml:id="A83029-001-a-2650">printed</w>
      <w lemma="and" pos="cc" xml:id="A83029-001-a-2660">and</w>
      <w lemma="publish" pos="vvn" xml:id="A83029-001-a-2670">published</w>
      <pc xml:id="A83029-001-a-2680">:</pc>
     </p>
     <closer xml:id="A83029-e180">
      <signed xml:id="A83029-e190">
       <w lemma="H" pos="sy" xml:id="A83029-001-a-2690">H</w>
       <pc xml:id="A83029-001-a-2700">:</pc>
       <w lemma="Elsing" pos="nn1" reg="Elsing" xml:id="A83029-001-a-2710">Elsynge</w>
       <pc xml:id="A83029-001-a-2720">,</pc>
       <w lemma="cler." pos="ab" xml:id="A83029-001-a-2730">Cler.</w>
       <w lemma="parl." pos="ab" xml:id="A83029-001-a-2740">Parl.</w>
       <w lemma="d." pos="ab" xml:id="A83029-001-a-2750">D.</w>
       <w lemma="com." pos="ab" xml:id="A83029-001-a-2760">Com.</w>
       <pc unit="sentence" xml:id="A83029-001-a-2770"/>
      </signed>
     </closer>
    </div>
   </div>
  </body>
  <back xml:id="A83029-e200">
   <div type="colophon" xml:id="A83029-e210">
    <p xml:id="A83029-e220">
     <w lemma="london" pos="nn1" xml:id="A83029-001-a-2780">London</w>
     <pc xml:id="A83029-001-a-2790">,</pc>
     <w lemma="print" pos="vvn" xml:id="A83029-001-a-2800">Printed</w>
     <w lemma="for" pos="acp" xml:id="A83029-001-a-2810">for</w>
     <hi xml:id="A83029-e230">
      <w lemma="Edward" pos="nn1" xml:id="A83029-001-a-2820">Edward</w>
      <w lemma="husband" pos="n1" xml:id="A83029-001-a-2830">Husband</w>
      <pc xml:id="A83029-001-a-2840">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A83029-001-a-2850">Printer</w>
     <w lemma="to" pos="acp" xml:id="A83029-001-a-2860">to</w>
     <w lemma="the" pos="d" xml:id="A83029-001-a-2870">the</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A83029-001-a-2880">Honorable</w>
     <w lemma="house" pos="n1" xml:id="A83029-001-a-2890">House</w>
     <w lemma="of" pos="acp" xml:id="A83029-001-a-2900">of</w>
     <w lemma="commons" pos="n2" xml:id="A83029-001-a-2910">COMMONS</w>
     <pc unit="sentence" xml:id="A83029-001-a-2920">.</pc>
     <w lemma="sept." pos="ab" xml:id="A83029-001-a-2930">Sept.</w>
     <w lemma="28." pos="crd" xml:id="A83029-001-a-2940">28.</w>
     <w lemma="1647." pos="crd" xml:id="A83029-001-a-2950">1647.</w>
     <pc unit="sentence" xml:id="A83029-001-a-2960"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
