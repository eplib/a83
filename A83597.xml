<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83597">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Votes of Parliament touching the book commonly called The Racovian catechism.</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83597 of text R211458 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.16[45]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83597</idno>
    <idno type="STC">Wing E2453</idno>
    <idno type="STC">Thomason 669.f.16[45]</idno>
    <idno type="STC">ESTC R211458</idno>
    <idno type="EEBO-CITATION">99870183</idno>
    <idno type="PROQUEST">99870183</idno>
    <idno type="VID">163200</idno>
    <idno type="PROQUESTGOID">2240927059</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83597)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163200)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f16[45])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Votes of Parliament touching the book commonly called The Racovian catechism.</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by John Field, Printer to the Parliament of England,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1652.</date>
     </publicationStmt>
     <notesStmt>
      <note>Ordering it to be burnt.</note>
      <note>Order to print dated: Friday the Second of April, 1652. Signed: Hen: Scobell, Cleric. Parliamenti.</note>
      <note>With Parliamentary seal at head of text.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Racovian Catechism -- Censorship -- England -- Early works to 1800.</term>
     <term>Catechisms, Polish -- Early works to 1800.</term>
     <term>Book burning -- England -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83597</ep:tcp>
    <ep:estc> R211458</ep:estc>
    <ep:stc> (Thomason 669.f.16[45]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Votes of Parliament touching the book commonly called The Racovian catechism. ...</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1652</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>253</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-11</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-11</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83597-e10">
  <body xml:id="A83597-e20">
   <pb facs="tcp:163200:1" rend="simple:additions" xml:id="A83597-001-a"/>
   <div type="order_of_Parliament" xml:id="A83597-e30">
    <head xml:id="A83597-e40">
     <figure xml:id="A83597-e50"/>
     <lb xml:id="A83597-e60"/>
     <w lemma="vote" pos="n2" xml:id="A83597-001-a-0010">Votes</w>
     <w lemma="of" pos="acp" xml:id="A83597-001-a-0020">of</w>
     <w lemma="parliament" pos="n1" xml:id="A83597-001-a-0030">Parliament</w>
     <w lemma="touch" pos="vvg" xml:id="A83597-001-a-0040">Touching</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-0050">the</w>
     <w lemma="book" pos="n1" xml:id="A83597-001-a-0060">Book</w>
     <w lemma="common" pos="av-j" xml:id="A83597-001-a-0070">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A83597-001-a-0080">called</w>
     <hi xml:id="A83597-e70">
      <w lemma="the" pos="d" xml:id="A83597-001-a-0090">The</w>
      <w lemma="racovian" pos="jnn" xml:id="A83597-001-a-0100">RACOVIAN</w>
      <w lemma="catechism" pos="n1" xml:id="A83597-001-a-0110">CATECHISM</w>
      <pc unit="sentence" xml:id="A83597-001-a-0120">.</pc>
     </hi>
    </head>
    <argument xml:id="A83597-e80">
     <p xml:id="A83597-e90">
      <w lemma="mr." pos="ab" rend="hi" xml:id="A83597-001-a-0130">Mr.</w>
      <w lemma="millington" pos="nn1" xml:id="A83597-001-a-0140">Millington</w>
      <hi xml:id="A83597-e110">
       <w lemma="report" pos="vvz" xml:id="A83597-001-a-0150">Reports</w>
       <w lemma="from" pos="acp" xml:id="A83597-001-a-0160">from</w>
       <w lemma="the" pos="d" xml:id="A83597-001-a-0170">the</w>
       <w lemma="committee" pos="n1" xml:id="A83597-001-a-0180">Committee</w>
       <w lemma="to" pos="acp" xml:id="A83597-001-a-0190">to</w>
       <w lemma="who" pos="crq" xml:id="A83597-001-a-0200">whom</w>
       <w lemma="the" pos="d" xml:id="A83597-001-a-0210">the</w>
       <w lemma="book" pos="n1" xml:id="A83597-001-a-0220">Book</w>
       <pc join="right" xml:id="A83597-001-a-0230">(</pc>
       <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A83597-001-a-0240">Entituled</w>
       <pc xml:id="A83597-001-a-0250">,</pc>
      </hi>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0260">Catechesis</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0270">Ecclesiarum</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0280">quae</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0290">in</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0300">Regno</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0310">Poloniae</w>
      <pc xml:id="A83597-001-a-0320">,</pc>
      <hi xml:id="A83597-e120">
       <w lemma="etc." pos="ab" xml:id="A83597-001-a-0330">&amp;c.</w>
       <w lemma="common" pos="av-j" xml:id="A83597-001-a-0340">commonly</w>
       <w lemma="call" pos="vvn" xml:id="A83597-001-a-0350">called</w>
      </hi>
      <w lemma="I" pos="pns" xml:id="A83597-001-a-0360">I</w>
      <w lemma="he" pos="pns" xml:id="A83597-001-a-0370">he</w>
      <w lemma="racovian" pos="jnn" xml:id="A83597-001-a-0380">Racovian</w>
      <w lemma="catechism" pos="n1" xml:id="A83597-001-a-0390">Catechism</w>
      <pc xml:id="A83597-001-a-0400">)</pc>
      <hi xml:id="A83597-e130">
       <w lemma="be" pos="vvd" xml:id="A83597-001-a-0410">was</w>
       <w lemma="refer" pos="vvn" xml:id="A83597-001-a-0420">referred</w>
       <pc xml:id="A83597-001-a-0430">,</pc>
       <w lemma="several" pos="j" xml:id="A83597-001-a-0440">several</w>
       <w lemma="passage" pos="n2" xml:id="A83597-001-a-0450">Passages</w>
       <w lemma="in" pos="acp" xml:id="A83597-001-a-0460">in</w>
       <w lemma="the" pos="d" xml:id="A83597-001-a-0470">the</w>
       <w lemma="say" pos="j-vn" xml:id="A83597-001-a-0480">said</w>
       <w lemma="book" pos="n1" xml:id="A83597-001-a-0490">Book</w>
       <pc xml:id="A83597-001-a-0500">,</pc>
       <w lemma="which" pos="crq" xml:id="A83597-001-a-0510">which</w>
       <w lemma="be" pos="vvd" xml:id="A83597-001-a-0520">were</w>
       <w lemma="now" pos="av" xml:id="A83597-001-a-0530">now</w>
       <w lemma="read" pos="vvn" xml:id="A83597-001-a-0540">Read</w>
       <pc unit="sentence" xml:id="A83597-001-a-0550">.</pc>
      </hi>
     </p>
    </argument>
    <opener xml:id="A83597-e140">
     <w lemma="resolve" pos="j-vn" xml:id="A83597-001-a-0560">Resolved</w>
     <w lemma="upon" pos="acp" xml:id="A83597-001-a-0570">upon</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-0580">the</w>
     <w lemma="question" pos="n1" xml:id="A83597-001-a-0590">Question</w>
     <w lemma="by" pos="acp" xml:id="A83597-001-a-0600">by</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-0610">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83597-001-a-0620">Parliament</w>
     <pc xml:id="A83597-001-a-0630">,</pc>
    </opener>
    <p xml:id="A83597-e150">
     <w lemma="that" pos="cs" rend="decorinit" xml:id="A83597-001-a-0640">THat</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-0650">the</w>
     <w lemma="book" pos="n1" xml:id="A83597-001-a-0660">Book</w>
     <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A83597-001-a-0670">Entituled</w>
     <pc xml:id="A83597-001-a-0680">,</pc>
     <foreign xml:id="A83597-e160" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0690">Catechesis</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0700">Ecclesiarum</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0710">quae</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0720">in</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0730">Regno</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-0740">Poloniae</w>
      <pc xml:id="A83597-001-a-0750">,</pc>
     </foreign>
     <w lemma="etc." pos="ab" xml:id="A83597-001-a-0760">&amp;c.</w>
     <w lemma="common" pos="av-j" xml:id="A83597-001-a-0770">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A83597-001-a-0780">called</w>
     <hi xml:id="A83597-e170">
      <w lemma="the" pos="d" xml:id="A83597-001-a-0790">The</w>
      <w lemma="racovian" pos="jnn" xml:id="A83597-001-a-0800">Racovian</w>
      <w lemma="catechism" pos="n1" xml:id="A83597-001-a-0810">Catechism</w>
      <pc xml:id="A83597-001-a-0820">,</pc>
     </hi>
     <w lemma="do" pos="vvz" xml:id="A83597-001-a-0830">doth</w>
     <w lemma="contain" pos="vvi" xml:id="A83597-001-a-0840">contain</w>
     <w lemma="matter" pos="n2" xml:id="A83597-001-a-0850">matters</w>
     <w lemma="that" pos="cs" xml:id="A83597-001-a-0860">that</w>
     <w lemma="be" pos="vvb" xml:id="A83597-001-a-0870">are</w>
     <w lemma="blasphemous" pos="j" xml:id="A83597-001-a-0880">Blasphemous</w>
     <pc xml:id="A83597-001-a-0890">,</pc>
     <w lemma="erroneous" pos="j" reg="erroneous" xml:id="A83597-001-a-0900">Erronious</w>
     <w lemma="and" pos="cc" xml:id="A83597-001-a-0910">and</w>
     <w lemma="scandalous" pos="j" xml:id="A83597-001-a-0920">Scandalous</w>
     <pc unit="sentence" xml:id="A83597-001-a-0930">.</pc>
    </p>
    <p xml:id="A83597-e180">
     <hi xml:id="A83597-e190">
      <w lemma="resolve" pos="j-vn" xml:id="A83597-001-a-0940">Resolved</w>
      <w lemma="upon" pos="acp" xml:id="A83597-001-a-0950">upon</w>
      <w lemma="the" pos="d" xml:id="A83597-001-a-0960">the</w>
      <w lemma="question" pos="n1" xml:id="A83597-001-a-0970">Question</w>
      <w lemma="by" pos="acp" xml:id="A83597-001-a-0980">by</w>
      <w lemma="the" pos="d" xml:id="A83597-001-a-0990">the</w>
      <w lemma="parliament" pos="n1" xml:id="A83597-001-a-1000">Parliament</w>
      <pc xml:id="A83597-001-a-1010">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A83597-001-a-1020">That</w>
     <w lemma="all" pos="d" xml:id="A83597-001-a-1030">all</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1040">the</w>
     <w lemma="print" pos="j-vn" xml:id="A83597-001-a-1050">printed</w>
     <w lemma="copy" pos="n2" xml:id="A83597-001-a-1060">Copies</w>
     <w lemma="of" pos="acp" xml:id="A83597-001-a-1070">of</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1080">the</w>
     <w lemma="book" pos="n1" xml:id="A83597-001-a-1090">Book</w>
     <pc xml:id="A83597-001-a-1100">,</pc>
     <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A83597-001-a-1110">Entituled</w>
     <pc xml:id="A83597-001-a-1120">,</pc>
     <foreign xml:id="A83597-e200" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1130">Catechesis</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1140">Ecclesiarum</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1150">quae</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1160">in</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1170">Regno</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1180">Poloniae</w>
      <pc xml:id="A83597-001-a-1190">,</pc>
     </foreign>
     <w lemma="etc." pos="ab" xml:id="A83597-001-a-1200">&amp;c.</w>
     <w lemma="common" pos="av-j" xml:id="A83597-001-a-1210">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A83597-001-a-1220">called</w>
     <hi xml:id="A83597-e210">
      <w lemma="the" pos="d" xml:id="A83597-001-a-1230">The</w>
      <w lemma="racovian" pos="jnn" xml:id="A83597-001-a-1240">Racovian</w>
      <w lemma="catechism" pos="n1" xml:id="A83597-001-a-1250">Catechism</w>
      <pc xml:id="A83597-001-a-1260">,</pc>
     </hi>
     <w lemma="be" pos="vvb" xml:id="A83597-001-a-1270">be</w>
     <w lemma="burn" pos="vvn" xml:id="A83597-001-a-1280">burnt</w>
     <pc unit="sentence" xml:id="A83597-001-a-1290">.</pc>
    </p>
    <p xml:id="A83597-e220">
     <hi xml:id="A83597-e230">
      <w lemma="resolve" pos="j-vn" xml:id="A83597-001-a-1300">Resolved</w>
      <w lemma="upon" pos="acp" xml:id="A83597-001-a-1310">upon</w>
      <w lemma="the" pos="d" xml:id="A83597-001-a-1320">the</w>
      <w lemma="question" pos="n1" xml:id="A83597-001-a-1330">Question</w>
      <w lemma="by" pos="acp" xml:id="A83597-001-a-1340">by</w>
      <w lemma="the" pos="d" xml:id="A83597-001-a-1350">the</w>
      <w lemma="parliament" pos="n1" xml:id="A83597-001-a-1360">Parliament</w>
      <pc xml:id="A83597-001-a-1370">,</pc>
     </hi>
     <w lemma="that" pos="cs" xml:id="A83597-001-a-1380">That</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1390">the</w>
     <w lemma="sheriff" pos="n2" xml:id="A83597-001-a-1400">Sheriffs</w>
     <w lemma="of" pos="acp" xml:id="A83597-001-a-1410">of</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A83597-001-a-1420">London</w>
     <w lemma="and" pos="cc" xml:id="A83597-001-a-1430">and</w>
     <w lemma="Middlesex" pos="nn1" rend="hi" xml:id="A83597-001-a-1440">Middlesex</w>
     <w lemma="be" pos="vvb" xml:id="A83597-001-a-1450">be</w>
     <w lemma="authorize" pos="vvn" xml:id="A83597-001-a-1460">authorized</w>
     <w lemma="and" pos="cc" xml:id="A83597-001-a-1470">and</w>
     <w lemma="require" pos="vvn" xml:id="A83597-001-a-1480">required</w>
     <w lemma="to" pos="prt" xml:id="A83597-001-a-1490">to</w>
     <w lemma="seize" pos="vvi" xml:id="A83597-001-a-1500">seize</w>
     <w lemma="all" pos="d" xml:id="A83597-001-a-1510">all</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1520">the</w>
     <w lemma="print" pos="j-vn" xml:id="A83597-001-a-1530">printed</w>
     <w lemma="copy" pos="n2" xml:id="A83597-001-a-1540">Copies</w>
     <w lemma="of" pos="acp" xml:id="A83597-001-a-1550">of</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1560">the</w>
     <w lemma="book" pos="n1" xml:id="A83597-001-a-1570">Book</w>
     <pc xml:id="A83597-001-a-1580">,</pc>
     <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A83597-001-a-1590">Entituled</w>
     <pc xml:id="A83597-001-a-1600">,</pc>
     <foreign xml:id="A83597-e260" xml:lang="lat">
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1610">Catechesis</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1620">Ecclesiarum</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1630">quae</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1640">in</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1650">Regno</w>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-1660">Poloniae</w>
      <pc xml:id="A83597-001-a-1670">,</pc>
     </foreign>
     <w lemma="etc." pos="ab" xml:id="A83597-001-a-1680">&amp;c.</w>
     <w lemma="common" pos="av-j" xml:id="A83597-001-a-1690">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A83597-001-a-1700">called</w>
     <hi xml:id="A83597-e270">
      <w lemma="the" pos="d" xml:id="A83597-001-a-1710">The</w>
      <w lemma="racovian" pos="jnn" xml:id="A83597-001-a-1720">Racovian</w>
      <w lemma="catechism" pos="n1" xml:id="A83597-001-a-1730">Catechism</w>
      <pc xml:id="A83597-001-a-1740">,</pc>
     </hi>
     <w lemma="wheresoever" pos="crq" xml:id="A83597-001-a-1750">wheresoever</w>
     <w lemma="they" pos="pns" xml:id="A83597-001-a-1760">they</w>
     <w lemma="shall" pos="vmb" xml:id="A83597-001-a-1770">shall</w>
     <w lemma="be" pos="vvi" xml:id="A83597-001-a-1780">be</w>
     <w lemma="find" pos="vvn" xml:id="A83597-001-a-1790">found</w>
     <pc xml:id="A83597-001-a-1800">,</pc>
     <w lemma="and" pos="cc" xml:id="A83597-001-a-1810">and</w>
     <w lemma="cause" pos="vvi" xml:id="A83597-001-a-1820">cause</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1830">the</w>
     <w lemma="same" pos="d" xml:id="A83597-001-a-1840">same</w>
     <w lemma="to" pos="prt" xml:id="A83597-001-a-1850">to</w>
     <w lemma="be" pos="vvi" xml:id="A83597-001-a-1860">be</w>
     <w lemma="burn" pos="vvn" xml:id="A83597-001-a-1870">burnt</w>
     <w lemma="at" pos="acp" xml:id="A83597-001-a-1880">at</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1890">the</w>
     <w lemma="old" pos="j" xml:id="A83597-001-a-1900">Old</w>
     <w lemma="exchange" pos="n1" xml:id="A83597-001-a-1910">Exchange</w>
     <hi xml:id="A83597-e280">
      <w lemma="London" pos="nn1" xml:id="A83597-001-a-1920">London</w>
      <pc xml:id="A83597-001-a-1930">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A83597-001-a-1940">and</w>
     <w lemma="in" pos="acp" xml:id="A83597-001-a-1950">in</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-1960">the</w>
     <w lemma="new" pos="j" xml:id="A83597-001-a-1970">New</w>
     <w lemma="palace" pos="n1" xml:id="A83597-001-a-1980">Palace</w>
     <w lemma="at" pos="acp" xml:id="A83597-001-a-1990">at</w>
     <hi xml:id="A83597-e290">
      <w lemma="Westminster" pos="nn1" xml:id="A83597-001-a-2000">Westminster</w>
      <pc xml:id="A83597-001-a-2010">,</pc>
     </hi>
     <w lemma="on" pos="acp" xml:id="A83597-001-a-2020">on</w>
     <w lemma="Tuesday" pos="nn1" xml:id="A83597-001-a-2030">Tuesday</w>
     <w lemma="and" pos="cc" xml:id="A83597-001-a-2040">and</w>
     <w lemma="thursday" pos="nn1" xml:id="A83597-001-a-2050">Thursday</w>
     <w lemma="next" pos="ord" xml:id="A83597-001-a-2060">next</w>
     <pc unit="sentence" xml:id="A83597-001-a-2070">.</pc>
    </p>
   </div>
   <div type="license" xml:id="A83597-e300">
    <opener xml:id="A83597-e310">
     <dateline xml:id="A83597-e320">
      <date xml:id="A83597-e330">
       <w lemma="Friday" pos="nn1" xml:id="A83597-001-a-2080">Friday</w>
       <w lemma="the" pos="d" xml:id="A83597-001-a-2090">the</w>
       <w lemma="second" pos="ord" xml:id="A83597-001-a-2100">Second</w>
       <w lemma="of" pos="acp" xml:id="A83597-001-a-2110">of</w>
       <hi xml:id="A83597-e340">
        <w lemma="April" pos="nn1" xml:id="A83597-001-a-2120">April</w>
        <pc xml:id="A83597-001-a-2130">,</pc>
       </hi>
       <w lemma="1652." pos="crd" xml:id="A83597-001-a-2140">1652.</w>
       <pc unit="sentence" xml:id="A83597-001-a-2150"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="A83597-e350">
     <w lemma="resolve" pos="j-vn" xml:id="A83597-001-a-2160">REsolved</w>
     <w lemma="by" pos="acp" xml:id="A83597-001-a-2170">by</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-2180">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83597-001-a-2190">Parliament</w>
     <pc xml:id="A83597-001-a-2200">,</pc>
     <w lemma="that" pos="cs" xml:id="A83597-001-a-2210">That</w>
     <w lemma="these" pos="d" xml:id="A83597-001-a-2220">these</w>
     <w lemma="vote" pos="n2" xml:id="A83597-001-a-2230">Votes</w>
     <w lemma="be" pos="vvb" xml:id="A83597-001-a-2240">be</w>
     <w lemma="forthwith" pos="av" xml:id="A83597-001-a-2250">forthwith</w>
     <w lemma="print" pos="vvn" xml:id="A83597-001-a-2260">Printed</w>
     <w lemma="and" pos="cc" xml:id="A83597-001-a-2270">and</w>
     <w lemma="publish" pos="vvn" xml:id="A83597-001-a-2280">Published</w>
     <pc unit="sentence" xml:id="A83597-001-a-2290">.</pc>
    </p>
    <closer xml:id="A83597-e360">
     <signed xml:id="A83597-e370">
      <w lemma="hen" pos="ab" xml:id="A83597-001-a-2300">Hen:</w>
      <w lemma="Scobell" pos="nn1" xml:id="A83597-001-a-2320">Scobell</w>
      <pc xml:id="A83597-001-a-2330">,</pc>
      <w lemma="cleric" pos="n1" xml:id="A83597-001-a-2340">Cleric</w>
      <pc unit="sentence" xml:id="A83597-001-a-2350">.</pc>
      <w lemma="n/a" pos="fla" xml:id="A83597-001-a-2360">Parliamenti</w>
      <pc unit="sentence" xml:id="A83597-001-a-2370">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A83597-e380">
   <div type="colophon" xml:id="A83597-e390">
    <p xml:id="A83597-e400">
     <hi xml:id="A83597-e410">
      <w lemma="london" pos="nn1" xml:id="A83597-001-a-2380">London</w>
      <pc xml:id="A83597-001-a-2390">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A83597-001-a-2400">Printed</w>
     <w lemma="by" pos="acp" xml:id="A83597-001-a-2410">by</w>
     <hi xml:id="A83597-e420">
      <w lemma="John" pos="nn1" xml:id="A83597-001-a-2420">John</w>
      <w lemma="field" pos="nn1" xml:id="A83597-001-a-2430">Field</w>
      <pc xml:id="A83597-001-a-2440">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A83597-001-a-2450">Printer</w>
     <w lemma="to" pos="acp" xml:id="A83597-001-a-2460">to</w>
     <w lemma="the" pos="d" xml:id="A83597-001-a-2470">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83597-001-a-2480">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A83597-001-a-2490">of</w>
     <hi xml:id="A83597-e430">
      <w lemma="England" pos="nn1" xml:id="A83597-001-a-2500">England</w>
      <pc unit="sentence" xml:id="A83597-001-a-2510">.</pc>
     </hi>
     <w lemma="1652." pos="crd" xml:id="A83597-001-a-2520">1652.</w>
     <pc unit="sentence" xml:id="A83597-001-a-2530"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
