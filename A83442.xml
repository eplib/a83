<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83442">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Die Veneris, 6 Decembr. 1650. Resolved by the Parliament, that no Captain shall absent himself from his charge, without leave from his field-officer; ....</title>
    <author>England and Wales. Parliament.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83442 of text R212058 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.15[67]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83442</idno>
    <idno type="STC">Wing E2256B</idno>
    <idno type="STC">Thomason 669.f.15[67]</idno>
    <idno type="STC">ESTC R212058</idno>
    <idno type="EEBO-CITATION">99870711</idno>
    <idno type="PROQUEST">99870711</idno>
    <idno type="VID">163140</idno>
    <idno type="PROQUESTGOID">2240953285</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83442)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163140)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f15[67])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Die Veneris, 6 Decembr. 1650. Resolved by the Parliament, that no Captain shall absent himself from his charge, without leave from his field-officer; ....</title>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by Edward Husband and Iohn Field, Printers to the Parliament of England,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1650.</date>
     </publicationStmt>
     <notesStmt>
      <note>Title from caption and opening words of text.</note>
      <note>Order to print signed: Hen: Scobell, Cleric. Parliamenti.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- Militia -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83442</ep:tcp>
    <ep:estc> R212058</ep:estc>
    <ep:stc> (Thomason 669.f.15[67]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>Die Veneris, 6 Decembr. 1650. Resolved by the Parliament, that no Captain shall absent himself from his charge, without leave from his field</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1650</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>150</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-10</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-11</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-11</date>
    <label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83442-e10">
  <body xml:id="A83442-e20">
   <pb facs="tcp:163140:1" rend="simple:additions" xml:id="A83442-001-a"/>
   <div type="text" xml:id="A83442-e30">
    <head xml:id="A83442-e40">
     <figure xml:id="A83442-e50">
      <figDesc xml:id="A83442-e60">blazon or coat of arms incorporating the Commonwealth Flag (1649-1651)</figDesc>
     </figure>
     <date xml:id="A83442-e70">
      <w lemma="n/a" pos="fla" xml:id="A83442-001-a-0010">Die</w>
      <w lemma="n/a" pos="fla" xml:id="A83442-001-a-0020">Veneris</w>
      <pc xml:id="A83442-001-a-0030">,</pc>
      <w lemma="6" pos="crd" xml:id="A83442-001-a-0040">6</w>
      <w lemma="Decembr" pos="nn1" xml:id="A83442-001-a-0050">Decembr</w>
      <pc xml:id="A83442-001-a-0060">.</pc>
      <w lemma="1650." pos="crd" xml:id="A83442-001-a-0070">1650.</w>
      <pc unit="sentence" xml:id="A83442-001-a-0080"/>
     </date>
    </head>
    <opener xml:id="A83442-e80">
     <w lemma="resolve" pos="j-vn" xml:id="A83442-001-a-0090">Resolved</w>
     <w lemma="by" pos="acp" xml:id="A83442-001-a-0100">by</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-0110">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83442-001-a-0120">Parliament</w>
     <pc xml:id="A83442-001-a-0130">,</pc>
    </opener>
    <p xml:id="A83442-e90">
     <w lemma="that" pos="cs" xml:id="A83442-001-a-0140">THat</w>
     <w lemma="no" pos="dx" xml:id="A83442-001-a-0150">no</w>
     <w lemma="captain" pos="n1" xml:id="A83442-001-a-0160">Captain</w>
     <w lemma="shall" pos="vmb" xml:id="A83442-001-a-0170">shall</w>
     <w lemma="absent" pos="vvi" xml:id="A83442-001-a-0180">absent</w>
     <w lemma="himself" pos="pr" xml:id="A83442-001-a-0190">himself</w>
     <w lemma="from" pos="acp" xml:id="A83442-001-a-0200">from</w>
     <w lemma="his" pos="po" xml:id="A83442-001-a-0210">his</w>
     <w lemma="charge" pos="n1" xml:id="A83442-001-a-0220">Charge</w>
     <pc xml:id="A83442-001-a-0230">,</pc>
     <w lemma="without" pos="acp" xml:id="A83442-001-a-0240">without</w>
     <w lemma="leave" pos="n1" xml:id="A83442-001-a-0250">leave</w>
     <w lemma="from" pos="acp" xml:id="A83442-001-a-0260">from</w>
     <w lemma="his" pos="po" xml:id="A83442-001-a-0270">his</w>
     <w lemma="field-officer" pos="n1" xml:id="A83442-001-a-0280">Field-Officer</w>
     <pc xml:id="A83442-001-a-0290">;</pc>
     <w lemma="and" pos="cc" xml:id="A83442-001-a-0300">and</w>
     <w lemma="in" pos="acp" xml:id="A83442-001-a-0310">in</w>
     <w lemma="such" pos="d" xml:id="A83442-001-a-0320">such</w>
     <w lemma="his" pos="po" xml:id="A83442-001-a-0330">his</w>
     <w lemma="absence" pos="n1" xml:id="A83442-001-a-0340">absence</w>
     <pc xml:id="A83442-001-a-0350">,</pc>
     <w lemma="leave" pos="vvg" xml:id="A83442-001-a-0360">leaving</w>
     <w lemma="his" pos="po" xml:id="A83442-001-a-0370">his</w>
     <w lemma="lieutenant" pos="n1" xml:id="A83442-001-a-0380">Lieutenant</w>
     <w lemma="with" pos="acp" xml:id="A83442-001-a-0390">with</w>
     <w lemma="his" pos="po" xml:id="A83442-001-a-0400">his</w>
     <w lemma="charge" pos="n1" xml:id="A83442-001-a-0410">Charge</w>
     <pc xml:id="A83442-001-a-0420">,</pc>
     <w lemma="for" pos="acp" xml:id="A83442-001-a-0430">for</w>
     <w lemma="who" pos="crq" xml:id="A83442-001-a-0440">whom</w>
     <w lemma="he" pos="pns" xml:id="A83442-001-a-0450">he</w>
     <w lemma="will" pos="vmb" xml:id="A83442-001-a-0460">will</w>
     <w lemma="be" pos="vvi" xml:id="A83442-001-a-0470">be</w>
     <w lemma="answerable" pos="j" xml:id="A83442-001-a-0480">answerable</w>
     <w lemma="touch" pos="vvg" xml:id="A83442-001-a-0490">touching</w>
     <w lemma="such" pos="d" xml:id="A83442-001-a-0500">such</w>
     <w lemma="injury" pos="n2" xml:id="A83442-001-a-0510">Injuries</w>
     <w lemma="and" pos="cc" xml:id="A83442-001-a-0520">and</w>
     <w lemma="damage" pos="n1" xml:id="A83442-001-a-0530">Damage</w>
     <w lemma="as" pos="acp" xml:id="A83442-001-a-0540">as</w>
     <w lemma="shall" pos="vmb" reg="shall" xml:id="A83442-001-a-0550">shal</w>
     <w lemma="be" pos="vvi" xml:id="A83442-001-a-0560">be</w>
     <w lemma="do" pos="vvn" xml:id="A83442-001-a-0570">done</w>
     <w lemma="to" pos="acp" xml:id="A83442-001-a-0580">to</w>
     <pc xml:id="A83442-001-a-0590">,</pc>
     <w lemma="or" pos="cc" xml:id="A83442-001-a-0600">or</w>
     <w lemma="sustain" pos="vvn" xml:id="A83442-001-a-0610">sustained</w>
     <w lemma="by" pos="acp" xml:id="A83442-001-a-0620">by</w>
     <w lemma="any" pos="d" xml:id="A83442-001-a-0630">any</w>
     <w lemma="of" pos="acp" xml:id="A83442-001-a-0640">of</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-0650">the</w>
     <w lemma="people" pos="n1" xml:id="A83442-001-a-0660">People</w>
     <w lemma="by" pos="acp" xml:id="A83442-001-a-0670">by</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-0680">the</w>
     <w lemma="soldier" pos="n2" xml:id="A83442-001-a-0690">Soldiers</w>
     <w lemma="under" pos="acp" xml:id="A83442-001-a-0700">under</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-0710">the</w>
     <w lemma="command" pos="n1" xml:id="A83442-001-a-0720">Command</w>
     <w lemma="of" pos="acp" xml:id="A83442-001-a-0730">of</w>
     <w lemma="such" pos="d" xml:id="A83442-001-a-0740">such</w>
     <w lemma="captain" pos="n1" xml:id="A83442-001-a-0750">Captain</w>
     <pc xml:id="A83442-001-a-0760">;</pc>
     <w lemma="and" pos="cc" xml:id="A83442-001-a-0770">And</w>
     <w lemma="that" pos="cs" xml:id="A83442-001-a-0780">that</w>
     <w lemma="it" pos="pn" xml:id="A83442-001-a-0790">it</w>
     <w lemma="be" pos="vvb" xml:id="A83442-001-a-0800">be</w>
     <w lemma="refer" pos="vvn" xml:id="A83442-001-a-0810">referred</w>
     <w lemma="to" pos="acp" xml:id="A83442-001-a-0820">to</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-0830">the</w>
     <w lemma="council" pos="n1" reg="council" xml:id="A83442-001-a-0840">Councel</w>
     <w lemma="of" pos="acp" xml:id="A83442-001-a-0850">of</w>
     <w lemma="state" pos="n1" xml:id="A83442-001-a-0860">State</w>
     <w lemma="to" pos="prt" xml:id="A83442-001-a-0870">to</w>
     <w lemma="take" pos="vvi" xml:id="A83442-001-a-0880">take</w>
     <w lemma="especial" pos="j" xml:id="A83442-001-a-0890">especial</w>
     <w lemma="care" pos="n1" xml:id="A83442-001-a-0900">care</w>
     <w lemma="that" pos="cs" xml:id="A83442-001-a-0910">that</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-0920">the</w>
     <w lemma="act" pos="n1" xml:id="A83442-001-a-0930">Act</w>
     <w lemma="for" pos="acp" xml:id="A83442-001-a-0940">for</w>
     <w lemma="prevent" pos="vvg" xml:id="A83442-001-a-0950">preventing</w>
     <w lemma="of" pos="acp" xml:id="A83442-001-a-0960">of</w>
     <w lemma="freequarter" pos="n1" reg="freequarter" xml:id="A83442-001-a-0970">Free-Quarter</w>
     <pc xml:id="A83442-001-a-0980">,</pc>
     <w lemma="and" pos="cc" xml:id="A83442-001-a-0990">and</w>
     <w lemma="likewise" pos="av" xml:id="A83442-001-a-1000">likewise</w>
     <w lemma="this" pos="d" xml:id="A83442-001-a-1010">this</w>
     <w lemma="vote" pos="n1" xml:id="A83442-001-a-1020">Vote</w>
     <pc xml:id="A83442-001-a-1030">,</pc>
     <w lemma="be" pos="vvb" xml:id="A83442-001-a-1040">be</w>
     <w lemma="effectual" pos="av-j" xml:id="A83442-001-a-1050">effectually</w>
     <w lemma="put" pos="vvn" xml:id="A83442-001-a-1060">put</w>
     <w lemma="in" pos="acp" xml:id="A83442-001-a-1070">in</w>
     <w lemma="execution" pos="n1" xml:id="A83442-001-a-1080">Execution</w>
     <pc unit="sentence" xml:id="A83442-001-a-1090">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A83442-e100">
   <div type="license" xml:id="A83442-e110">
    <p xml:id="A83442-e120">
     <w lemma="order" pos="j-vn" xml:id="A83442-001-a-1100">ORdered</w>
     <w lemma="by" pos="acp" xml:id="A83442-001-a-1110">by</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-1120">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83442-001-a-1130">Parliament</w>
     <pc xml:id="A83442-001-a-1140">,</pc>
     <w lemma="that" pos="cs" xml:id="A83442-001-a-1150">That</w>
     <w lemma="this" pos="d" xml:id="A83442-001-a-1160">this</w>
     <w lemma="vote" pos="n1" xml:id="A83442-001-a-1170">Vote</w>
     <w lemma="be" pos="vvb" xml:id="A83442-001-a-1180">be</w>
     <w lemma="forthwith" pos="av" xml:id="A83442-001-a-1190">forthwith</w>
     <w lemma="print" pos="vvn" xml:id="A83442-001-a-1200">Printed</w>
     <w lemma="and" pos="cc" xml:id="A83442-001-a-1210">and</w>
     <w lemma="publish" pos="vvn" xml:id="A83442-001-a-1220">Published</w>
     <pc unit="sentence" xml:id="A83442-001-a-1230">.</pc>
    </p>
    <closer xml:id="A83442-e130">
     <signed xml:id="A83442-e140">
      <w lemma="hen" pos="ab" xml:id="A83442-001-a-1240">Hen:</w>
      <w lemma="Scobell" pos="nn1" xml:id="A83442-001-a-1260">Scobell</w>
      <pc xml:id="A83442-001-a-1270">,</pc>
      <w lemma="cleric" pos="n1" xml:id="A83442-001-a-1280">Cleric</w>
      <pc unit="sentence" xml:id="A83442-001-a-1290">.</pc>
      <w lemma="n/a" pos="fla" xml:id="A83442-001-a-1300">Parliamenti</w>
      <pc unit="sentence" xml:id="A83442-001-a-1310">.</pc>
     </signed>
    </closer>
   </div>
   <div type="colophon" xml:id="A83442-e150">
    <p xml:id="A83442-e160">
     <hi xml:id="A83442-e170">
      <w lemma="london" pos="nn1" xml:id="A83442-001-a-1320">London</w>
      <pc xml:id="A83442-001-a-1330">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A83442-001-a-1340">Printed</w>
     <w lemma="by" pos="acp" xml:id="A83442-001-a-1350">by</w>
     <hi xml:id="A83442-e180">
      <w lemma="Edward" pos="nn1" xml:id="A83442-001-a-1360">Edward</w>
      <w lemma="husband" pos="n1" xml:id="A83442-001-a-1370">Husband</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A83442-001-a-1380">and</w>
     <hi xml:id="A83442-e190">
      <w lemma="John" pos="nn1" reg="John" xml:id="A83442-001-a-1390">Iohn</w>
      <w lemma="field" pos="nn1" xml:id="A83442-001-a-1400">Field</w>
      <pc xml:id="A83442-001-a-1410">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A83442-001-a-1420">Printers</w>
     <w lemma="to" pos="acp" xml:id="A83442-001-a-1430">to</w>
     <w lemma="the" pos="d" xml:id="A83442-001-a-1440">the</w>
     <w lemma="parliament" pos="n1" xml:id="A83442-001-a-1450">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A83442-001-a-1460">of</w>
     <hi xml:id="A83442-e200">
      <w lemma="England" pos="nn1" xml:id="A83442-001-a-1470">England</w>
      <pc xml:id="A83442-001-a-1480">,</pc>
     </hi>
     <w lemma="1650." pos="crd" xml:id="A83442-001-a-1490">1650.</w>
     <pc unit="sentence" xml:id="A83442-001-a-1500"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
