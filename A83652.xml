<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A83652">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A certificate of what hath been done upon the poll-money, as well upon the act of poll-money, as upon the order of review of the same.</title>
    <author>England and Wales. Parliament. House of Commons.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A83652 of text R205388 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.3[21]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A83652</idno>
    <idno type="STC">Wing E2535A</idno>
    <idno type="STC">Thomason 669.f.3[21]</idno>
    <idno type="STC">ESTC R205388</idno>
    <idno type="EEBO-CITATION">99864787</idno>
    <idno type="PROQUEST">99864787</idno>
    <idno type="VID">160579</idno>
    <idno type="PROQUESTGOID">2248530186</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A83652)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160579)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f3[21])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A certificate of what hath been done upon the poll-money, as well upon the act of poll-money, as upon the order of review of the same.</title>
      <author>England and Wales. Parliament. House of Commons.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Robert Barker, printer to the Kings most Excellent Majesty: and by the assignes of John Bill,</publisher>
      <pubPlace>Imprinted at London :</pubPlace>
      <date>1641.</date>
     </publicationStmt>
     <notesStmt>
      <note>"The form of certificate with blanks alluded to in Ordinance of 29 Nov."--Steele.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Poll tax -- Great Britain -- Early works to 1800.</term>
     <term>Taxation -- England -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A83652</ep:tcp>
    <ep:estc> R205388</ep:estc>
    <ep:stc> (Thomason 669.f.3[21]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>A certificate of what hath been done upon the poll-money, as well upon the act of poll-money, as upon the order of review of the same.</ep:title>
    <ep:author>England and Wales. Parliament. </ep:author>
    <ep:publicationYear>1641</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>314</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-11</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-11</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-12</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-12</date>
    <label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A83652-e10">
  <body xml:id="A83652-e20">
   <pb facs="tcp:160579:1" rend="simple:additions" xml:id="A83652-001-a"/>
   <div type="text" xml:id="A83652-e30">
    <head xml:id="A83652-e40">
     <pc xml:id="A83652-001-a-0010">¶</pc>
     <w lemma="a" pos="d" xml:id="A83652-001-a-0020">A</w>
     <w lemma="certificate" pos="n1" xml:id="A83652-001-a-0030">Certificate</w>
     <w lemma="of" pos="acp" xml:id="A83652-001-a-0040">of</w>
     <w lemma="what" pos="crq" xml:id="A83652-001-a-0050">what</w>
     <w lemma="have" pos="vvz" xml:id="A83652-001-a-0060">hath</w>
     <w lemma="be" pos="vvn" xml:id="A83652-001-a-0070">been</w>
     <w lemma="do" pos="vvn" xml:id="A83652-001-a-0080">done</w>
     <w lemma="upon" pos="acp" xml:id="A83652-001-a-0090">upon</w>
     <w lemma="the" pos="d" xml:id="A83652-001-a-0100">the</w>
     <w lemma="poll-money" pos="n1" xml:id="A83652-001-a-0110">Poll-money</w>
     <pc xml:id="A83652-001-a-0120">,</pc>
     <w lemma="as" pos="acp" xml:id="A83652-001-a-0130">as</w>
     <w lemma="well" pos="av" xml:id="A83652-001-a-0140">well</w>
     <w lemma="upon" pos="acp" xml:id="A83652-001-a-0150">upon</w>
     <w lemma="the" pos="d" xml:id="A83652-001-a-0160">the</w>
     <w lemma="act" pos="n1" xml:id="A83652-001-a-0170">Act</w>
     <w lemma="of" pos="acp" xml:id="A83652-001-a-0180">of</w>
     <w lemma="poll-money" pos="n1" xml:id="A83652-001-a-0190">Poll-money</w>
     <pc xml:id="A83652-001-a-0200">,</pc>
     <w lemma="as" pos="acp" xml:id="A83652-001-a-0210">as</w>
     <w lemma="upon" pos="acp" xml:id="A83652-001-a-0220">upon</w>
     <w lemma="the" pos="d" xml:id="A83652-001-a-0230">the</w>
     <w lemma="order" pos="n1" xml:id="A83652-001-a-0240">Order</w>
     <w lemma="of" pos="acp" xml:id="A83652-001-a-0250">of</w>
     <w lemma="review" pos="n1" xml:id="A83652-001-a-0260">Review</w>
     <w lemma="of" pos="acp" xml:id="A83652-001-a-0270">of</w>
     <w lemma="the" pos="d" xml:id="A83652-001-a-0280">the</w>
     <w lemma="same" pos="d" xml:id="A83652-001-a-0290">same</w>
     <pc unit="sentence" xml:id="A83652-001-a-0300">.</pc>
    </head>
    <p xml:id="A83652-e50">
     <w lemma="commons" pos="n2" xml:id="A83652-001-a-0310">COMMONS</w>
     <w lemma="house" pos="n1" xml:id="A83652-001-a-0320">HOUSE</w>
     <pc unit="sentence" xml:id="A83652-001-a-0330">.</pc>
    </p>
    <p xml:id="A83652-e60">
     <hi xml:id="A83652-e70">
      <w lemma="n/a" pos="fla" xml:id="A83652-001-a-0340">IMprimis</w>
      <pc xml:id="A83652-001-a-0350">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A83652-001-a-0360">The</w>
     <w lemma="charge" pos="n1" xml:id="A83652-001-a-0370">charge</w>
     <w lemma="of" pos="acp" xml:id="A83652-001-a-0380">of</w>
     <w lemma="each" pos="d" xml:id="A83652-001-a-0390">each</w>
     <w lemma="several" pos="j" reg="several" xml:id="A83652-001-a-0400">severall</w>
     <w lemma="division" pos="n1" xml:id="A83652-001-a-0410">division</w>
     <w lemma="amount" pos="vvz" xml:id="A83652-001-a-0420">amounteth</w>
     <w lemma="in" pos="acp" xml:id="A83652-001-a-0430">in</w>
     <w lemma="the" pos="d" xml:id="A83652-001-a-0440">the</w>
     <w lemma="total" pos="j" reg="total" xml:id="A83652-001-a-0450">totall</w>
     <w lemma="sum" pos="n1" xml:id="A83652-001-a-0460">sum</w>
     <w lemma="unto" pos="acp" xml:id="A83652-001-a-0470">unto</w>
     <pc xml:id="A83652-001-a-0480">—</pc>
    </p>
    <list xml:id="A83652-e80">
     <head xml:id="A83652-e90">
      <w lemma="the" pos="d" xml:id="A83652-001-a-0490">The</w>
      <w lemma="discharge" pos="n1" xml:id="A83652-001-a-0500">discharge</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-0510">of</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-0520">the</w>
      <w lemma="say" pos="j-vn" xml:id="A83652-001-a-0530">said</w>
      <w lemma="total" pos="j" reg="total" xml:id="A83652-001-a-0540">totall</w>
      <w lemma="sum" pos="n1" xml:id="A83652-001-a-0550">sum</w>
      <w lemma="appear" pos="vvz" xml:id="A83652-001-a-0560">appeareth</w>
      <w lemma="in" pos="acp" xml:id="A83652-001-a-0570">in</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-0580">the</w>
      <w lemma="particular" pos="n2-j" xml:id="A83652-001-a-0590">particulars</w>
      <w lemma="follow" pos="vvg" xml:id="A83652-001-a-0600">following</w>
      <pc xml:id="A83652-001-a-0610">,</pc>
      <pc join="right" xml:id="A83652-001-a-0620">(</pc>
      <w lemma="viz" pos="av" xml:id="A83652-001-a-0630">Viz</w>
      <pc unit="sentence" xml:id="A83652-001-a-0640">)</pc>
     </head>
     <item xml:id="A83652-e100">
      <w lemma="1" pos="crd" xml:id="A83652-001-a-0650">1</w>
      <w lemma="money" pos="n1" xml:id="A83652-001-a-0660">Money</w>
      <w lemma="pay" pos="vvn" xml:id="A83652-001-a-0670">paid</w>
      <w lemma="into" pos="acp" xml:id="A83652-001-a-0680">into</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-0690">the</w>
      <w lemma="chamber" pos="n1" xml:id="A83652-001-a-0700">Chamber</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-0710">of</w>
      <w lemma="London" pos="nn1" rend="hi" xml:id="A83652-001-a-0720">London</w>
      <pc xml:id="A83652-001-a-0730">—</pc>
     </item>
     <item xml:id="A83652-e120">
      <w lemma="2" pos="crd" xml:id="A83652-001-a-0740">2</w>
      <w lemma="money" pos="n1" xml:id="A83652-001-a-0750">Money</w>
      <w lemma="pay" pos="vvn" xml:id="A83652-001-a-0760">paid</w>
      <w lemma="for" pos="acp" xml:id="A83652-001-a-0770">for</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-0780">the</w>
      <w lemma="collect" pos="vvg" xml:id="A83652-001-a-0790">Collecting</w>
      <w lemma="and" pos="cc" xml:id="A83652-001-a-0800">and</w>
      <w lemma="pay" pos="vvg" xml:id="A83652-001-a-0810">paying</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-0820">of</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-0830">the</w>
      <w lemma="same" pos="d" xml:id="A83652-001-a-0840">same</w>
      <pc xml:id="A83652-001-a-0850">,</pc>
      <w lemma="according" pos="j" xml:id="A83652-001-a-0860">according</w>
      <w lemma="to" pos="acp" xml:id="A83652-001-a-0870">to</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-0880">the</w>
      <w lemma="statute" pos="n1" xml:id="A83652-001-a-0890">Statute</w>
      <w lemma="or" pos="cc" xml:id="A83652-001-a-0900">or</w>
      <w lemma="order" pos="n1" xml:id="A83652-001-a-0910">Order</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-0920">of</w>
      <w lemma="review" pos="n1" xml:id="A83652-001-a-0930">review</w>
      <pc xml:id="A83652-001-a-0940">—</pc>
     </item>
     <item xml:id="A83652-e130">
      <w lemma="3" pos="crd" xml:id="A83652-001-a-0950">3</w>
      <w lemma="in" pos="acp" xml:id="A83652-001-a-0960">In</w>
      <w lemma="certificate" pos="n2" xml:id="A83652-001-a-0970">Certificates</w>
      <w lemma="see" pos="vvn" xml:id="A83652-001-a-0980">seen</w>
      <w lemma="and" pos="cc" xml:id="A83652-001-a-0990">and</w>
      <w lemma="allow" pos="vvn" xml:id="A83652-001-a-1000">allowed</w>
      <w lemma="according" pos="j" xml:id="A83652-001-a-1010">according</w>
      <w lemma="to" pos="acp" xml:id="A83652-001-a-1020">to</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-1030">the</w>
      <w lemma="say" pos="j-vn" xml:id="A83652-001-a-1040">said</w>
      <w lemma="statute" pos="n1" xml:id="A83652-001-a-1050">Statute</w>
      <pc xml:id="A83652-001-a-1060">,</pc>
      <w lemma="the" pos="d" xml:id="A83652-001-a-1070">the</w>
      <w lemma="sum" pos="n1" xml:id="A83652-001-a-1080">sum</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-1090">of</w>
      <w lemma="money" pos="n1" xml:id="A83652-001-a-1100">money</w>
      <w lemma="be" pos="vvz" xml:id="A83652-001-a-1110">is</w>
      <pc xml:id="A83652-001-a-1120">—</pc>
     </item>
     <item xml:id="A83652-e140">
      <w lemma="4" pos="crd" xml:id="A83652-001-a-1130">4</w>
      <w lemma="in" pos="acp" xml:id="A83652-001-a-1140">In</w>
      <w lemma="pretend" pos="j-vn" xml:id="A83652-001-a-1150">pretended</w>
      <w lemma="certificate" pos="n2" xml:id="A83652-001-a-1160">Certificates</w>
      <w lemma="neither" pos="avx-d" xml:id="A83652-001-a-1170">neither</w>
      <w lemma="see" pos="vvn" xml:id="A83652-001-a-1180">seen</w>
      <w lemma="nor" pos="ccx" xml:id="A83652-001-a-1190">nor</w>
      <w lemma="allow" pos="vvn" xml:id="A83652-001-a-1200">allowed</w>
      <pc xml:id="A83652-001-a-1210">,</pc>
      <w lemma="the" pos="d" xml:id="A83652-001-a-1220">the</w>
      <w lemma="sum" pos="n1" xml:id="A83652-001-a-1230">sum</w>
      <w lemma="be" pos="vvz" xml:id="A83652-001-a-1240">is</w>
      <pc xml:id="A83652-001-a-1250">—</pc>
     </item>
     <item xml:id="A83652-e150">
      <w lemma="5" pos="crd" xml:id="A83652-001-a-1260">5</w>
      <w lemma="money" pos="n2" xml:id="A83652-001-a-1270">Moneys</w>
      <w lemma="uncollected" pos="j" xml:id="A83652-001-a-1280">uncollected</w>
      <pc xml:id="A83652-001-a-1290">,</pc>
      <w lemma="and" pos="cc" xml:id="A83652-001-a-1300">and</w>
      <w lemma="not" pos="xx" xml:id="A83652-001-a-1310">not</w>
      <w lemma="distrain" pos="vvn" xml:id="A83652-001-a-1320">distreined</w>
      <w lemma="by" pos="acp" xml:id="A83652-001-a-1330">by</w>
      <w lemma="reason" pos="n1" xml:id="A83652-001-a-1340">reason</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-1350">of</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-1360">the</w>
      <w lemma="poverty" pos="n1" reg="poverty" xml:id="A83652-001-a-1370">povertie</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-1380">of</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-1390">the</w>
      <w lemma="party" pos="n1" reg="party" xml:id="A83652-001-a-1400">partie</w>
      <w lemma="assess" pos="vvn" xml:id="A83652-001-a-1410">assessed</w>
      <pc xml:id="A83652-001-a-1420">—</pc>
     </item>
     <item xml:id="A83652-e160">
      <w lemma="6" pos="crd" xml:id="A83652-001-a-1430">6</w>
      <w lemma="such" pos="d" xml:id="A83652-001-a-1440">Such</w>
      <w lemma="as" pos="acp" xml:id="A83652-001-a-1450">as</w>
      <w lemma="be" pos="vvb" xml:id="A83652-001-a-1460">are</w>
      <w lemma="dead" pos="j" xml:id="A83652-001-a-1470">dead</w>
      <w lemma="and" pos="cc" xml:id="A83652-001-a-1480">and</w>
      <w lemma="be" pos="vvd" xml:id="A83652-001-a-1490">were</w>
      <w lemma="able" pos="j" xml:id="A83652-001-a-1500">able</w>
      <w lemma="to" pos="prt" xml:id="A83652-001-a-1510">to</w>
      <w lemma="pay" pos="vvi" xml:id="A83652-001-a-1520">pay</w>
      <w lemma="in" pos="acp" xml:id="A83652-001-a-1530">in</w>
      <w lemma="their" pos="po" xml:id="A83652-001-a-1540">their</w>
      <w lemma="life" pos="n1" xml:id="A83652-001-a-1550">life</w>
      <w lemma="time" pos="n1" xml:id="A83652-001-a-1560">time</w>
      <pc xml:id="A83652-001-a-1570">—</pc>
     </item>
     <item xml:id="A83652-e170">
      <w lemma="7" pos="crd" xml:id="A83652-001-a-1580">7</w>
      <w lemma="refuser" pos="n2" xml:id="A83652-001-a-1590">Refusers</w>
      <pc xml:id="A83652-001-a-1600">,</pc>
      <w lemma="that" pos="cs" xml:id="A83652-001-a-1610">that</w>
      <w lemma="do" pos="vvb" xml:id="A83652-001-a-1620">do</w>
      <w lemma="not" pos="xx" xml:id="A83652-001-a-1630">not</w>
      <pc xml:id="A83652-001-a-1640">,</pc>
      <w lemma="or" pos="cc" xml:id="A83652-001-a-1650">or</w>
      <w lemma="will" pos="vmb" xml:id="A83652-001-a-1660">will</w>
      <w lemma="not" pos="xx" xml:id="A83652-001-a-1670">not</w>
      <w lemma="pay" pos="vvi" xml:id="A83652-001-a-1680">pay</w>
      <pc xml:id="A83652-001-a-1690">,</pc>
      <w lemma="although" pos="cs" xml:id="A83652-001-a-1700">although</w>
      <w lemma="able" pos="j" xml:id="A83652-001-a-1710">able</w>
      <w lemma="and" pos="cc" xml:id="A83652-001-a-1720">and</w>
      <w lemma="sufficient" pos="j" xml:id="A83652-001-a-1730">sufficient</w>
      <pc xml:id="A83652-001-a-1740">,</pc>
      <w lemma="who" pos="crq" xml:id="A83652-001-a-1750">whose</w>
      <w lemma="name" pos="n2" xml:id="A83652-001-a-1760">names</w>
      <w lemma="with" pos="acp" xml:id="A83652-001-a-1770">with</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-1780">the</w>
      <w lemma="sum" pos="n2" xml:id="A83652-001-a-1790">sums</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-1800">of</w>
      <w lemma="money" pos="n1" xml:id="A83652-001-a-1810">money</w>
      <w lemma="assess" pos="vvn" xml:id="A83652-001-a-1820">assessed</w>
      <w lemma="upon" pos="acp" xml:id="A83652-001-a-1830">upon</w>
      <w lemma="they" pos="pno" xml:id="A83652-001-a-1840">them</w>
      <w lemma="be" pos="vvb" xml:id="A83652-001-a-1850">are</w>
      <w lemma="in" pos="acp" xml:id="A83652-001-a-1860">in</w>
      <w lemma="a" pos="d" xml:id="A83652-001-a-1870">a</w>
      <w lemma="schedule" pos="n1" xml:id="A83652-001-a-1880">Schedule</w>
      <w lemma="annex" pos="vvn" xml:id="A83652-001-a-1890">annexed</w>
      <w lemma="hereunto" pos="av" xml:id="A83652-001-a-1900">hereunto</w>
      <pc xml:id="A83652-001-a-1910">,</pc>
      <w lemma="amount" pos="vvz" xml:id="A83652-001-a-1920">amounteth</w>
      <w lemma="unto" pos="acp" xml:id="A83652-001-a-1930">unto</w>
      <pc xml:id="A83652-001-a-1940">—</pc>
     </item>
     <item xml:id="A83652-e180">
      <w lemma="8" pos="crd" xml:id="A83652-001-a-1950">8</w>
      <hi xml:id="A83652-e190">
       <w lemma="memorandum" pos="n1" xml:id="A83652-001-a-1960">Memorandum</w>
       <pc xml:id="A83652-001-a-1970">,</pc>
      </hi>
      <w lemma="all" pos="d" xml:id="A83652-001-a-1980">All</w>
      <w lemma="that" pos="cs" xml:id="A83652-001-a-1990">that</w>
      <w lemma="be" pos="vvb" xml:id="A83652-001-a-2000">are</w>
      <w lemma="assess" pos="vvn" xml:id="A83652-001-a-2010">assessed</w>
      <w lemma="under" pos="acp" xml:id="A83652-001-a-2020">under</w>
      <w lemma="twelve" pos="crd" xml:id="A83652-001-a-2030">twelve</w>
      <w lemma="penny" pos="n2" xml:id="A83652-001-a-2040">pence</w>
      <w lemma="be" pos="vvb" xml:id="A83652-001-a-2050">are</w>
      <w lemma="not" pos="xx" xml:id="A83652-001-a-2060">not</w>
      <w lemma="to" pos="prt" xml:id="A83652-001-a-2070">to</w>
      <w lemma="be" pos="vvi" xml:id="A83652-001-a-2080">be</w>
      <w lemma="particular" pos="av-j" xml:id="A83652-001-a-2090">particularly</w>
      <w lemma="name" pos="vvn" xml:id="A83652-001-a-2100">named</w>
      <w lemma="or" pos="cc" xml:id="A83652-001-a-2110">or</w>
      <w lemma="write" pos="vvn" xml:id="A83652-001-a-2120">written</w>
      <pc xml:id="A83652-001-a-2130">,</pc>
      <w lemma="but" pos="acp" xml:id="A83652-001-a-2140">but</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-2150">the</w>
      <w lemma="total" pos="j" reg="total" xml:id="A83652-001-a-2160">totall</w>
      <w lemma="sum" pos="n1" xml:id="A83652-001-a-2170">sum</w>
      <w lemma="in" pos="acp" xml:id="A83652-001-a-2180">in</w>
      <w lemma="every" pos="d" xml:id="A83652-001-a-2190">every</w>
      <w lemma="parish" pos="n1" xml:id="A83652-001-a-2200">Parish</w>
      <w lemma="or" pos="cc" xml:id="A83652-001-a-2210">or</w>
      <w lemma="township" pos="n1" xml:id="A83652-001-a-2220">Township</w>
      <w lemma="therein" pos="av" xml:id="A83652-001-a-2230">therein</w>
      <w lemma="set" pos="vvn" xml:id="A83652-001-a-2240">set</w>
      <w lemma="down" pos="acp" xml:id="A83652-001-a-2250">down</w>
      <pc xml:id="A83652-001-a-2260">,</pc>
      <w lemma="the" pos="d" xml:id="A83652-001-a-2270">The</w>
      <w lemma="gross" pos="j" reg="gross" xml:id="A83652-001-a-2280">grosse</w>
      <w lemma="sum" pos="n1" xml:id="A83652-001-a-2290">sum</w>
      <w lemma="whereof" pos="crq" xml:id="A83652-001-a-2300">whereof</w>
      <w lemma="be" pos="vvz" xml:id="A83652-001-a-2310">is</w>
      <pc xml:id="A83652-001-a-2320">—</pc>
     </item>
     <item xml:id="A83652-e200">
      <w lemma="9" pos="crd" xml:id="A83652-001-a-2330">9</w>
      <w lemma="you" pos="pn" xml:id="A83652-001-a-2340">You</w>
      <w lemma="shall" pos="vmb" xml:id="A83652-001-a-2350">shall</w>
      <w lemma="likewise" pos="av" xml:id="A83652-001-a-2360">likewise</w>
      <w lemma="certify" pos="vvi" reg="certify" xml:id="A83652-001-a-2370">certifie</w>
      <w lemma="where" pos="crq" xml:id="A83652-001-a-2380">where</w>
      <w lemma="any" pos="d" xml:id="A83652-001-a-2390">any</w>
      <w lemma="considerable" pos="j" xml:id="A83652-001-a-2400">considerable</w>
      <w lemma="partiality" pos="n1" reg="partiality" xml:id="A83652-001-a-2410">partialitie</w>
      <w lemma="or" pos="cc" xml:id="A83652-001-a-2420">or</w>
      <w lemma="connivance" pos="n1" reg="connivance" xml:id="A83652-001-a-2430">connivence</w>
      <w lemma="have" pos="vvz" xml:id="A83652-001-a-2440">hath</w>
      <w lemma="be" pos="vvn" xml:id="A83652-001-a-2450">been</w>
      <w lemma="use" pos="vvn" xml:id="A83652-001-a-2460">used</w>
      <pc xml:id="A83652-001-a-2470">,</pc>
      <w lemma="either" pos="av-d" xml:id="A83652-001-a-2480">either</w>
      <w lemma="by" pos="acp" xml:id="A83652-001-a-2490">by</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-2500">the</w>
      <w lemma="commissioner" pos="n2" xml:id="A83652-001-a-2510">Commissioners</w>
      <w lemma="or" pos="cc" xml:id="A83652-001-a-2520">or</w>
      <w lemma="assessor" pos="n2" xml:id="A83652-001-a-2530">Assessors</w>
      <pc xml:id="A83652-001-a-2540">,</pc>
      <w lemma="and" pos="cc" xml:id="A83652-001-a-2550">and</w>
      <w lemma="in" pos="acp" xml:id="A83652-001-a-2560">in</w>
      <w lemma="such" pos="d" xml:id="A83652-001-a-2570">such</w>
      <w lemma="case" pos="n1" xml:id="A83652-001-a-2580">case</w>
      <w lemma="you" pos="pn" xml:id="A83652-001-a-2590">you</w>
      <w lemma="be" pos="vvb" xml:id="A83652-001-a-2600">are</w>
      <w lemma="to" pos="prt" xml:id="A83652-001-a-2610">to</w>
      <w lemma="certify" pos="vvi" reg="certify" xml:id="A83652-001-a-2620">certifie</w>
      <w lemma="the" pos="d" xml:id="A83652-001-a-2630">the</w>
      <w lemma="particular" pos="j" xml:id="A83652-001-a-2640">particular</w>
      <w lemma="person" pos="n2" xml:id="A83652-001-a-2650">persons</w>
      <w lemma="name" pos="n1" xml:id="A83652-001-a-2660">name</w>
      <w lemma="and" pos="cc" xml:id="A83652-001-a-2670">and</w>
      <w lemma="place" pos="n1" xml:id="A83652-001-a-2680">place</w>
      <w lemma="of" pos="acp" xml:id="A83652-001-a-2690">of</w>
      <w lemma="abode" pos="n1" xml:id="A83652-001-a-2700">abode</w>
      <pc xml:id="A83652-001-a-2710">,</pc>
      <w lemma="that" pos="cs" xml:id="A83652-001-a-2720">that</w>
      <w lemma="so" pos="av" xml:id="A83652-001-a-2730">so</w>
      <w lemma="such" pos="d" xml:id="A83652-001-a-2740">such</w>
      <w lemma="person" pos="n1" xml:id="A83652-001-a-2750">person</w>
      <w lemma="may" pos="vmb" xml:id="A83652-001-a-2760">may</w>
      <w lemma="receive" pos="vvi" xml:id="A83652-001-a-2770">receive</w>
      <w lemma="his" pos="po" xml:id="A83652-001-a-2780">his</w>
      <w lemma="condign" pos="j" reg="condign" xml:id="A83652-001-a-2790">condigne</w>
      <w lemma="censure" pos="n1" xml:id="A83652-001-a-2800">censure</w>
      <pc unit="sentence" xml:id="A83652-001-a-2810">.</pc>
     </item>
    </list>
    <closer xml:id="A83652-e210">
     <signed xml:id="A83652-e220">
      <w lemma="h." pos="ab" xml:id="A83652-001-a-2820">H.</w>
      <w lemma="Elsing" pos="nn1" reg="Elsing" xml:id="A83652-001-a-2830">Elsynge</w>
      <w lemma="cler." pos="ab" xml:id="A83652-001-a-2840">Cler.</w>
      <w lemma="parl." pos="ab" xml:id="A83652-001-a-2850">Parl.</w>
      <w lemma="d." pos="ab" xml:id="A83652-001-a-2860">D.</w>
      <w lemma="com." pos="ab" xml:id="A83652-001-a-2870">Com.</w>
      <pc unit="sentence" xml:id="A83652-001-a-2880"/>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A83652-e230">
   <div type="colophon" xml:id="A83652-e240">
    <p xml:id="A83652-e250">
     <pc xml:id="A83652-001-a-2890">¶</pc>
     <w lemma="imprint" pos="vvn" xml:id="A83652-001-a-2900">Imprinted</w>
     <w lemma="at" pos="acp" xml:id="A83652-001-a-2910">at</w>
     <w lemma="London" pos="nn1" xml:id="A83652-001-a-2920">London</w>
     <w lemma="by" pos="acp" xml:id="A83652-001-a-2930">by</w>
     <w lemma="Robert" pos="nn1" xml:id="A83652-001-a-2940">Robert</w>
     <w lemma="Barker" pos="nn1" xml:id="A83652-001-a-2950">Barker</w>
     <pc xml:id="A83652-001-a-2960">,</pc>
     <w lemma="printer" pos="n1" xml:id="A83652-001-a-2970">Printer</w>
     <w lemma="to" pos="acp" xml:id="A83652-001-a-2980">to</w>
     <w lemma="the" pos="d" xml:id="A83652-001-a-2990">the</w>
     <w lemma="king" pos="n2" xml:id="A83652-001-a-3000">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A83652-001-a-3010">most</w>
     <w lemma="excellent" pos="j" xml:id="A83652-001-a-3020">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A83652-001-a-3030">Majesty</w>
     <pc xml:id="A83652-001-a-3040">:</pc>
     <w lemma="and" pos="cc" xml:id="A83652-001-a-3050">And</w>
     <w lemma="by" pos="acp" xml:id="A83652-001-a-3060">by</w>
     <w lemma="the" pos="d" xml:id="A83652-001-a-3070">the</w>
     <w lemma="assign" pos="vvz" reg="assigns" xml:id="A83652-001-a-3080">Assignes</w>
     <w lemma="of" pos="acp" xml:id="A83652-001-a-3090">of</w>
     <w lemma="JOHN" pos="nn1" xml:id="A83652-001-a-3100">JOHN</w>
     <w lemma="bill" pos="nn1" xml:id="A83652-001-a-3110">BILL</w>
     <pc unit="sentence" xml:id="A83652-001-a-3120">.</pc>
     <w lemma="1641." pos="crd" xml:id="A83652-001-a-3130">1641.</w>
     <pc unit="sentence" xml:id="A83652-001-a-3140"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
